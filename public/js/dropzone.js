Dropzone.options.myAwesomeDropzone = {
	 autoProcessQueue : false,
	 acceptedFiles: ".png,.jpg,.jpeg,.PNG,.JPG,.JPEG",
	 init:function(){
	 	var submit = document.querySelector("#btn_upload");
	 	mydrop = this;



	 	submit.addEventListener('click', function(){
	 		if ($("#question_id_image").val() == "" || $("#question_id_image").val() == null) {
	 			swal("Oops!","Please save the question first before uploading image.","info");
	 			mydrop.removeAllFiles();
	 		}else{
	 			mydrop.processQueue();
	 		}
	 		
	 	});

	 	this.on('complete', function(){
	 		if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0 ) {
	 			var _this = this;
	 			_this.removeAllFiles();
	 			swal("Success","Image uploaded successfully!","success");
	 		}
	 	});
	 },
  	success: function(file, response){
        // alert(response);
        console.log(response);
     }
	};