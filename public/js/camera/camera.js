 function openQRCamera(node) {
     
       const file = document.querySelector('input[type=file]').files[0];
       const reader = new FileReader();

        reader.addEventListener("load", function () {
          // convert image file to base64 string
          // preview.src = reader.result;

           $("#image_preview").modal('show');
            $("#img_prevs").attr('src', reader.result);
        }, false);

        if (file) {
          reader.readAsDataURL(file);
        }
    }

  function showQRIntro() {
    return confirm("Do you want use your camera to take a picture?");
  }