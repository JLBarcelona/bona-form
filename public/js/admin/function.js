$("#form_add_new").on('submit', function(e){
		e.preventDefault();
		var url = $(this).attr('action');
		// alert(url);
		var form_name = $("#form_name");
		var form_id = $("#form_id");
		if(form_name.val() == "" || form_name.val() == null){
			form_name.focus();
			swal('Oops!','Enter form title!','error');
		}
		else{
			$.ajax({
				type:"POST",
				url:url,
				data:{form_name :form_name.val(), form_id: form_id.val()},
				dataType:'json',
				beforeSend:function(){
						$(".submit").attr('disabled',true);
						$(".submit img").show('fast');
				},
				success:function(response){
					// console.log(response);
					if (response.status == true && response.update  == true) {
						location.reload();
					}else if (response.status == true && response.update  == false) {
						window.location ="admin/form/" + response.id;
					}else{
						swal("Oops!","There\'s something wrong with the server!","error");
					}
					$(".submit").attr('disabled',false);
					$(".submit img").hide('slow');
				},
				error: function(error){
					console.log(error);
				}
			});
		}
	});



function delete_form(id,name,url){
	swal({
      title: "Are you sure?",
      text: "Do you want to delete this form \'"+name+"\' ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
     $.ajax({
			type:"POST",
			url:url,
			data:{form_id : id},
			dataType:'json',
			beforeSend:function(){
					$(".submit").attr('disabled',true);
					$(".submit img").show('fast');
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					// window.location ="admin/form/" + response.id;
						swal("Success",response.message,"success");
						$("#form_"+id).hide('slow');

				}else{
					swal("Oops!","There\'s something wrong with the server!","error");
				}
				$(".submit").attr('disabled',false);
				$(".submit img").hide('slow');
			},
			error: function(error){
				console.log(error);
			}
		});
    });
}

