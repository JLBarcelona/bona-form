
function do_preview(){
	$("img").on('click', function(){
		var src = $(this).attr("src");
		// console.log(src);
		$("#img_preview").attr("src", src);
		$("#preview_images").modal("show");
		$('#zoomer').zoom();
	});
}


$("img").on('click', function(){
	var src = $(this).attr("src");
	// console.log(src);
	$("#img_preview").attr("src", src);
	$("#preview_images").modal("show");
	$('#zoomer').zoom();
});


function previewFile() {
	const att = $("#input").val();
	const preview = $("#"+att+"_preview");
	const base64 = $("#"+att);
	const file = document.querySelector('input[type=file]').files[0];
	const reader = new FileReader();

	reader.addEventListener("load", function () {
		base64.val(reader.result);
		$("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mb-2" width="150">');
	}, false);

	if (file) {
		reader.readAsDataURL(file);
	}
}


function upload(){
	const att = $("#input").val();
	const preview = $("#"+att+"_preview");
	const base64 = $("#"+att).val();
	 preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">');
	 $("#modal_upload").modal('hide');
	 $("#input").val('');
	 $("#preview_img").html('');
	 do_preview();
}

function show_upload(param){
	// Preview
	$("#input").val(param);
	$("#modal_upload").modal('show');
}


function close(){
	 $("#modal_upload").modal('hide');
	 $("#input").val('');
		 $("#preview_img").html('');
}
