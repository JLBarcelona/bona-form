<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;


class Form extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'form_id';

    protected $table = 'bona_form';

    protected $appends = ['status'];

    protected $fillable = [
        'account_name','account_name2','account_name3','email_address','avatar', 'customer_service_phone', 'trademark_logo', 'cover_menu_left', 'menu_picture_left', 'cover_menu_center', 'menu_picture_center', 'cover_menu_right', 'menu_picture_right', 'is_seen', 'created_at', 'updated_at', 'deleted_at',
    ];

     public function getCreatedAtAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

    public function getStatusAttribute($value)
    {
        if ($this->is_seen == 0) {
            return '<span class="text-success"><b>New Request</b></span>';
        }else{
            return '<span class="text-default"><b>Viewed</b></span>';
        }
    }
}
