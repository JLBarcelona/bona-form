<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;


class Application extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'application_form_id';

    protected $table = 'bona_application_form';

    protected $appends = ['status'];


    protected $fillable = ['email_address','company_name','company_registration_number','company_registration_address','company_website','oa_operator','oa_operator_passport_number','oa_operator_mobile_number','oa_landline','wechat_officials_account_names', 'wechat_officials_account_names1', 'wechat_officials_account_names2','account_introduction','document','acra_biz_file','oa_passport_image','oa_nric_image','oa_months_telephone_receipt_image','application_form_image','trade_mark_authorization_image','pdpa_policy_true','pdpa_policy_others',
    'payment_wechat_account_annual_fee','nric_full_name', 'nric_number', 'designation', 'nric_company_name', 'uen','access_token','date_submitted','is_seen','created_at','updated_at','deleted_at'];

     public function getCreatedAtAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }

    public function getStatusAttribute($value)
    {
        if ($this->is_seen == 0) {
            return '<span class="text-success"><b>New Request</b></span>';
        }else{
            return '<span class="text-default"><b>Viewed</b></span>';
        }
    }
}
