<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\RequestMail;

use App\Form;
use App\Admin;
use App\Email;

use Validator;
use Auth;
use Str;
use Hash;
use Storage;

// RequestMail

class HomeController extends Controller
{
 	function take_form(){
 		return view('Form.index');
 	}


 	function edit_form($form_id){
 		$form = Form::where('form_id',$form_id)->firstOrFail();
 		$form->is_seen = 1;
 		$form->save();
 		return view('Form.edit', compact('form'));
 	}

  function view($form_id){
    $form = Form::where('form_id',$form_id)->firstOrFail();
 		$form->is_seen = 1;
 		$form->save();
 		return view('Form.edit', compact('form'));
  }

 	function submit($form_id = ''){
 		if (!empty($form_id)) {
	 		return view('Form.submit');
 		}else{
 			return redirect('form');
 		}
 	}


	// function get_image($image_64) {
	// 	$imageName = time().'.png';
	// 	Storage::disk('profile')->put('profile_picture/'.$imageName, base64_decode($image_64));
	// 	return $imageName;
	// }

 	function save_file_to($file,$name, $base_filename = ''){

    if (!empty($base_filename)) {
      $imageName = $base_filename.time().'.png';
    }else{
      $imageName = $name.time().'.png';
    }

	 	$image_parts = explode(";base64,", $file);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);

 		$image = str_replace('data:image/jpeg;base64,', '', $file);
    	$image = str_replace(' ', '+', $image);
		Storage::disk('local_path')->put('upload/'.$imageName, $image_base64);
		return $imageName;
 	}


 	 function is_unique($param,$tbl){
    	$result = ($param > 0) ? '|unique:'.$tbl : '';

    	return $result;
    }

 	function add_form(Request $request){
    $admin = Admin::where('user_type', 1)->first();
 		$status = false;
 		$avatar_path = '';
		$trademark_logo_path = '';
		$menu_picture_left_path = '';
		$menu_picture_right_path = '';

		$form_id= $request->get('form_id');
		$account_name = $request->get('account_name');
		$customer_service_phone = $request->get('customer_service_phone');

    $cover_menu_left1 = $request->get('cover_menu_left1');
    $cover_menu_left2 = $request->get('cover_menu_left2');
    $cover_menu_left3 = $request->get('cover_menu_left3');
    $cover_menu_left4 = $request->get('cover_menu_left4');
		$cover_menu_left_main = $request->get('cover_menu_left_main');

    $cover_menu_center1 = $request->get('cover_menu_center1');
    $cover_menu_center2 = $request->get('cover_menu_center2');
    $cover_menu_center3 = $request->get('cover_menu_center3');
    $cover_menu_center4 = $request->get('cover_menu_center4');
		$cover_menu_center_main = $request->get('cover_menu_center_main');

    $cover_menu_right1 = $request->get('cover_menu_right1');
    $cover_menu_right2 = $request->get('cover_menu_right2');
    $cover_menu_right3 = $request->get('cover_menu_right3');
    $cover_menu_right4 = $request->get('cover_menu_right4');
		$cover_menu_right_main = $request->get('cover_menu_right_main');

    $submit = $request->get('submit');
		$page = $request->get('page');
    $account_name2 = $request->get('account_name2');
    $account_name3 = $request->get('account_name3');


		// Imgaes
		$avatar = $request->get('avatar');
		$trademark_logo = $request->get('trademark_logo');
		$menu_picture_left = $request->get('menu_picture_left');
		$menu_picture_center = $request->get('menu_picture_center');
		$menu_picture_right = $request->get('menu_picture_right');


    $email_check = Form::where('account_name', $account_name)->whereNull('deleted_at')->where('form_id', '!=', $form_id)->count();
    $email_check2 = Form::where('account_name2', $account_name)->whereNull('deleted_at')->where('form_id', '!=', $form_id)->count();
		$email_check3 = Form::where('account_name3', $account_name)->whereNull('deleted_at')->where('form_id', '!=', $form_id)->count();

      $menu_picture_left_validator = (!empty($menu_picture_left)) ? 'required' : '';
      $menu_picture_center_validator = (!empty($menu_picture_center)) ? 'required' : '';
      $menu_picture_right_validator = (!empty($menu_picture_right)) ? 'required' : '';

      $cover_menu_left_validator = (empty($cover_menu_left) && $page == 1) ? 'required' : '';
      $cover_menu_center_validator = (empty($cover_menu_center) && $page == 1) ? 'required' : '';
      $cover_menu_right_validator = (empty($cover_menu_right) && $page == 1) ? 'required' : '';


		$validator = Validator::make($request->all(), [
			'account_name' => 'required'.$this->is_unique($email_check, 'bona_form'),
      'account_name2' => 'required'.$this->is_unique($email_check2, 'bona_form'),
      'account_name3' => 'required'.$this->is_unique($email_check3, 'bona_form'),
      'customer_service_phone' => 'required'
		]);

    $validator1 = Validator::make($request->all(), [
      'left_menu_picture_name' => $menu_picture_left_validator,
      'center_menu_picture_name' => $menu_picture_center_validator,
      'right_menu_picture_name' => $menu_picture_right_validator,
      'cover_menu_left' => $cover_menu_left_validator,
      'cover_menu_center' => $cover_menu_center_validator,
      'cover_menu_right' => $cover_menu_right_validator,
    ]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors() , 'submit' => 0]);
		}elseif($validator1->fails()){
      return response()->json(['status' => false, 'error' => $validator1->errors(), 'submit' => 1, 'page' => 2]);
    }else{
      $left_menu_picture_name = $request->get('left_menu_picture_name');
      $center_menu_picture_name = $request->get('center_menu_picture_name');
      $right_menu_picture_name = $request->get('right_menu_picture_name');

      $avatar_path =  (!empty($avatar))? $this->save_file_to($avatar,'avatar') : '';
      $trademark_logo_path =  (!empty($trademark_logo))? $this->save_file_to($trademark_logo,'trademarklogo', '') : '';
      $menu_picture_left_path =  (!empty($menu_picture_left))? $this->save_file_to($menu_picture_left,'menupictureleft', $left_menu_picture_name) : '';
      $menu_picture_center_path =  (!empty($menu_picture_center))? $this->save_file_to($menu_picture_center,'menupicturecenter', $center_menu_picture_name) : '';
      $menu_picture_right_path =  (!empty($menu_picture_right))? $this->save_file_to($menu_picture_right,'menupictureright', $right_menu_picture_name) : '';

      if (!empty($form_id)) {
        $form = Form::find($form_id);
        $form->account_name = $account_name;
        $form->account_name2 = $account_name2;
        $form->account_name3 = $account_name3;
        $form->customer_service_phone = $customer_service_phone;
        $form->cover_menu_left1 = $cover_menu_left1;
        $form->cover_menu_left2 = $cover_menu_left2;
        $form->cover_menu_left3 = $cover_menu_left3;
        $form->cover_menu_left4 = $cover_menu_left4;
        $form->cover_menu_left_main = $cover_menu_left_main;

        $form->cover_menu_center1 = $cover_menu_center1;
        $form->cover_menu_center2 = $cover_menu_center2;
        $form->cover_menu_center3 = $cover_menu_center3;
        $form->cover_menu_center4 = $cover_menu_center4;
        $form->cover_menu_center_main = $cover_menu_center_main;

        $form->cover_menu_right1 = $cover_menu_right1;
        $form->cover_menu_right2 = $cover_menu_right2;
        $form->cover_menu_right3 = $cover_menu_right3;
        $form->cover_menu_right4 = $cover_menu_right4;
        $form->cover_menu_right_main = $cover_menu_right_main;

        // Image
        (!empty($avatar_path))? $form->avatar = $avatar_path : '';
        (!empty($trademark_logo_path))? $form->trademark_logo = $trademark_logo_path : '';
        (!empty($menu_picture_left_path))? $form->menu_picture_left = $menu_picture_left_path : '';
        (!empty($menu_picture_center_path))? $form->menu_picture_center = $menu_picture_center_path : '';
        (!empty($menu_picture_right_path))? $form->menu_picture_right = $menu_picture_right_path : '';

        if($form->save()){
          return response()->json(['status' => true, 'message' => 'Form updated successfully!' , 'id' => $form->form_id, 'submit' => 2]);
        }
      }else{
        $form = new Form;
        $form->account_name = $account_name;
        $form->account_name2 = $account_name2;
        $form->account_name3 = $account_name3;

        $form->customer_service_phone = $customer_service_phone;
        // $form->cover_menu_left = $cover_menu_left;
        // $form->cover_menu_center = $cover_menu_center;
        // $form->cover_menu_right = $cover_menu_right;
        $form->cover_menu_left1 = $cover_menu_left1;
        $form->cover_menu_left2 = $cover_menu_left2;
        $form->cover_menu_left3 = $cover_menu_left3;
        $form->cover_menu_left4 = $cover_menu_left4;
        $form->cover_menu_left_main = $cover_menu_left_main;

        $form->cover_menu_center1 = $cover_menu_center1;
        $form->cover_menu_center2 = $cover_menu_center2;
        $form->cover_menu_center3 = $cover_menu_center3;
        $form->cover_menu_center4 = $cover_menu_center4;
        $form->cover_menu_center_main = $cover_menu_center_main;

        $form->cover_menu_right1 = $cover_menu_right1;
        $form->cover_menu_right2 = $cover_menu_right2;
        $form->cover_menu_right3 = $cover_menu_right3;
        $form->cover_menu_right4 = $cover_menu_right4;
        $form->cover_menu_right_main = $cover_menu_right_main;
        // Image
        $form->avatar = $avatar_path;
        $form->trademark_logo = $trademark_logo_path;
        $form->menu_picture_left = $menu_picture_left_path;
        $form->menu_picture_center = $menu_picture_center_path;
        $form->menu_picture_right = $menu_picture_right_path;

        if($form->save()){
          $to_name = $account_name;
          $url_link = $url_link = url('/form/view/'.$form->form_id.'');
          $data = array('name_admin' => $admin->name, 'name' => $to_name, 'url_link' => $url_link);
          return $this->send_email($data, $form->form_id);
        }
      }
    }
	}

  function send_email($data, $id){
    $status = 0;
    $admin_q = Email::whereNull('deleted_at');
    $total = $admin_q->count();
    $admin = $admin_q->get();
    foreach ($admin as $email) {
      $status++;
      Mail::to($email->email_address)->send(new RequestMail($data));
    }

    if ($status == $total) {
      return response()->json(['status' => true, 'progress' => false, 'message' => 'Form saved successfully!', 'id' => $id, 'submit' => 2]);
    }

  }

}
