<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\RequestMail;

use App\Application;
use App\Admin;
use App\Email;

use Validator;
use Auth;
use Str;
use Hash;
use Storage;

// RequestMail

class ApplicationController extends Controller
{
  function form_download($token){
    $apps = Application::whereNull('deleted_at')->where('access_token', $token);

    if ($apps->count() > 0) {
      $app = $apps->first();
      return view('Application.download_form', compact('app'));
    }else{
      return view('Application.download_form_error');
    }

  }

  function take_form(){
    $token = Str::random(20);
 		return view('Application.index', compact('token'));
 	}

  function delete_app($application_form_id){
  	$app = Application::find($application_form_id);
  	if($app->delete()){
  		return response()->json(['status' => true, 'message' => 'App deleted successfully!']);
  	}
  }

  function list_app(){
  	$app = Application::whereNull('deleted_at')->get();
  	return response()->json(['status' => true, 'data' => $app]);
  }


  function edit_form($application_form_id){
    $form = Application::where('application_form_id',$application_form_id)->firstOrFail();
    $form->is_seen = 1;
    $form->save();
    return view('Application.edit', compact('form'));
  }

 function view($application_form_id){
  $form = Application::where('application_form_id',$application_form_id)->firstOrFail();
  $form->is_seen = 1;
  $form->save();
  return view('Application.edit', compact('form'));
 }


  function save_file_to($file,$name){
    $imageName = $name.time().'.png';

    $image_parts = explode(";base64,", $file);
    $image_type_aux = explode("image/", $image_parts[0]);
    $image_type = $image_type_aux[1];
    $image_base64 = base64_decode($image_parts[1]);

    $image = str_replace('data:image/jpeg;base64,', '', $file);
    $image = str_replace(' ', '+', $image);
    Storage::disk('local_path')->put('upload/'.$imageName, $image_base64);
    return $imageName;
    }

   function is_unique($param,$tbl){
    $result = ($param > 0) ? '|unique:'.$tbl : '';

    return $result;
   }


  function add_form(Request $request){
    $admin = Admin::where('user_type', 1)->first();

		$application_form_id = $request->get('application_form_id');

    $submit = $request->get('submit');
    $page = $request->get('page');
    $email_address = $request->get('email_address');
    $company_name = $request->get('company_name');
    $company_registration_number = $request->get('company_registration_number');
    $company_registration_address = $request->get('company_registration_address');
    $company_website = $request->get('company_website');
    $oa_operator = $request->get('oa_operator');
    $oa_operator_passport_number = $request->get('oa_operator_passport_number');
    $oa_operator_mobile_number = $request->get('oa_operator_mobile_number');
    $oa_landline = $request->get('oa_landline');
    $wechat_officials_account_names = $request->get('wechat_officials_account_names');
    $account_introduction = $request->get('account_introduction');
    $wechat_officials_account_names1 = $request->get('wechat_officials_account_names1');
    $wechat_officials_account_names2 = $request->get('wechat_officials_account_names2');
    // Images
    $document = $request->get('document');
    $acra_biz_file = $request->get('acra_biz_file');
    $oa_passport_image = $request->get('oa_passport_image');
    $oa_nric_image = $request->get('oa_nric_image');
    $oa_months_telephone_receipt_image = $request->get('oa_months_telephone_receipt_image');
    $application_form_image = $request->get('application_form_image');
    $trade_mark_authorization_image = $request->get('trade_mark_authorization_image');
    // 5ft
    $pdpa_policy_true = $request->get('pdpa_policy_true');
    $pdpa_policy_others = $request->get('pdpa_policy_others');
    // last
    $payment_wechat_account_annual_fee = $request->get('payment_wechat_account_annual_fee');
    $access_token = $request->get('access_token');

    // New LAst part

    $nric_full_name = $request->get('nric_full_name');
    $nric_number = $request->get('nric_number');
    $designation = $request->get('designation');
    $nric_company_name = $request->get('nric_company_name');
    $uen = $request->get('uen');


		// $email_check = Application::where('email_address', $email_address)->whereNull('deleted_at')->where('application_form_id', '!=', $application_form_id)->count();
    $v1 = (empty($pdpa_policy_true)) ? 'required' : $pdpa_policy_true;
    $v2 = ($v1 == 'no' && empty($pdpa_policy_others)) ? 'required' : '';
    $main_v = ($v1 !== 'no')? $v1 : $v2;
    $lv = ($main_v == 'yes')? '' : $main_v;

    $validator = Validator::make($request->all(), [
      'pdpa_policy_others' => $lv,
      'nric_full_name' => 'required',
      'nric_number' => 'required',
      'designation' => 'required',
      'nric_company_name' => 'required',
      'uen' => 'required',
		]);

    // $validator2 = Validator::make($request->all(), [
    //   'nric_full_name' => 'required',
    //   'nric_number' => 'required',
    //   'designation' => 'required',
    //   'nric_company_name' => 'required',
    //   'uen' => 'required',
		// ]);


    // else if ($validator2->fails() && $page == 5) {
    //   return response()->json(['status' => false, 'error' => $validator->errors() , 'submit' => 1,  'page' => 6]);
    // }else if ($page == 5) {
    //   return response()->json(['status' => false, 'error' => $validator->errors() , 'submit' => 1,  'page' => 6]);
    // }

        if ($validator->fails()) {
          // $validator->errors()->add('pdpa_policy_others', 'This is a required question');
          return response()->json(['status' => false, 'error' => $validator->errors() , 'submit' => 0,  'page' => 5]);
        }else{

          $document_path = (!empty($document)) ? $this->save_file_to($document,'document') : '';
          $acra_biz_file_path = (!empty($acra_biz_file)) ? $this->save_file_to($acra_biz_file,'acra_biz_file') : '';
          $oa_passport_image_path = (!empty($oa_passport_image)) ? $this->save_file_to($oa_passport_image,'oa_passport_image') : '';
          $oa_nric_image_path = (!empty($oa_nric_image)) ? $this->save_file_to($oa_nric_image,'oa_nric_image') : '';
          $oa_months_telephone_receipt_image_path = (!empty($oa_months_telephone_receipt_image)) ? $this->save_file_to($oa_months_telephone_receipt_image,'oa_months_telephone_receipt_image') : '';
          $application_form_image_path = (!empty($application_form_image)) ? $this->save_file_to($application_form_image,'application_form_image') : '';
          $trade_mark_authorization_image_path = (!empty($trade_mark_authorization_image)) ? $this->save_file_to($trade_mark_authorization_image,'trade_mark_authorization_image') : '';
          $payment_wechat_account_annual_fee_path = (!empty($payment_wechat_account_annual_fee)) ? $this->save_file_to($payment_wechat_account_annual_fee,'payment_wechat_account_annual_fee') : '';

          $pdpa_check = ($pdpa_policy_true == 'yes') ? 1 : 0;

          if (!empty($application_form_id)) {
            // code...
            $app = Application::find($application_form_id);
            $app->email_address = $email_address;
            $app->company_name = $company_name;
            $app->company_registration_number = $company_registration_number;
            $app->company_registration_address = $company_registration_address;
            $app->company_website = $company_website;
            $app->oa_operator = $oa_operator;
            $app->oa_operator_passport_number = $oa_operator_passport_number;
            $app->oa_operator_mobile_number = $oa_operator_mobile_number;
            $app->oa_landline = $oa_landline;
            $app->wechat_officials_account_names = $wechat_officials_account_names;
            $app->wechat_officials_account_names1 = $wechat_officials_account_names1;
            $app->wechat_officials_account_names2 = $wechat_officials_account_names2;
            $app->account_introduction = $account_introduction;

            // Images

            (!empty($document_path)) ? $app->document = $document_path : '';
            (!empty($acra_biz_file_path)) ? $app->acra_biz_file = $acra_biz_file_path : '';
            (!empty($oa_passport_image_path)) ? $app->oa_passport_image = $oa_passport_image_path : '';
            (!empty($oa_nric_image_path)) ? $app->oa_nric_image = $oa_nric_image_path : '';
            (!empty($oa_months_telephone_receipt_image_path)) ? $app->oa_months_telephone_receipt_image = $oa_months_telephone_receipt_image_path : '';
            (!empty($application_form_image_path)) ? $app->application_form_image = $application_form_image_path : '';
            (!empty($trade_mark_authorization_image_path)) ? $app->trade_mark_authorization_image = $trade_mark_authorization_image_path : '';
            (!empty($payment_wechat_account_annual_fee_path)) ? $app->payment_wechat_account_annual_fee = $payment_wechat_account_annual_fee_path : '';

            // 5ft
            $app->pdpa_policy_true = $pdpa_check;
            $app->pdpa_policy_others = $pdpa_policy_others;
            // last
            // $app->payment_wechat_account_annual_fee = $payment_wechat_account_annual_fee;

            // New last
            $app->nric_full_name = $nric_full_name;
            $app->nric_number = $nric_number;
            $app->designation = $designation;
            $app->nric_company_name = $nric_company_name;
            $app->uen = $uen;


            if ($app->save()) {
              return response()->json(['status' => true, 'message' => 'Form updated successfully!', 'id' => $app->application_form_id , 'page' => 6]);
            }

          }else{
            $app = new Application;
            $app->email_address = $email_address;
            $app->company_name = $company_name;
            $app->company_registration_number = $company_registration_number;
            $app->company_registration_address = $company_registration_address;
            $app->company_website = $company_website;
            $app->oa_operator = $oa_operator;
            $app->oa_operator_passport_number = $oa_operator_passport_number;
            $app->oa_operator_mobile_number = $oa_operator_mobile_number;
            $app->oa_landline = $oa_landline;
            $app->wechat_officials_account_names = $wechat_officials_account_names;
            $app->wechat_officials_account_names1 = $wechat_officials_account_names1;
            $app->wechat_officials_account_names2 = $wechat_officials_account_names2;

            $app->account_introduction = $account_introduction;
            // Images
            $app->document = $document_path;
            $app->acra_biz_file = $acra_biz_file_path;
            $app->oa_passport_image = $oa_passport_image_path;
            $app->oa_nric_image = $oa_nric_image_path;
            $app->oa_months_telephone_receipt_image = $oa_months_telephone_receipt_image_path;
            $app->application_form_image = $application_form_image_path;
            $app->trade_mark_authorization_image = $trade_mark_authorization_image_path;
            $app->payment_wechat_account_annual_fee = $payment_wechat_account_annual_fee_path;
            // Token
            $app->access_token = $access_token;
            // 5ft
            $app->pdpa_policy_true = $pdpa_check;
            $app->pdpa_policy_others = $pdpa_policy_others;
            // last
            // $app->payment_wechat_account_annual_fee = $payment_wechat_account_annual_fee;
            $app->date_submitted = now();

            // New last
            $app->nric_full_name = $nric_full_name;
            $app->nric_number = $nric_number;
            $app->designation = $designation;
            $app->nric_company_name = $nric_company_name;
            $app->uen = $uen;

            if ($app->save()) {
              $to_name = $company_name;
              $url_link = $url_link = url('/application/view/'.$app->application_form_id.'');
              $data = array('name_admin' => $company_name, 'name' => $to_name, 'url_link' => $url_link);
              return $this->send_email($data, $app->application_form_id);
            }
          }
        }
	}

  function send_email($data, $id){
    $status = 0;
    $admin_q = Email::whereNull('deleted_at');
    $total = $admin_q->count();
    $admin = $admin_q->get();
    foreach ($admin as $email) {
      $status++;
      Mail::to($email->email_address)->send(new RequestMail($data));
    }

    if ($status == $total) {
      return response()->json(['status' => true, 'progress' => false, 'message' => 'Form saved successfully!', 'id' => $id, 'page' => 6]);
    }

  }

  function submit($application_form_id = ''){
 		if (!empty($application_form_id)) {
      $form = Application::where('application_form_id',$application_form_id)->firstOrFail();

	 		return view('Application.submit', compact('form'));
 		}else{
 			return redirect('application');
 		}
 	}


}
