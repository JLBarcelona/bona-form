<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\User;
use App\Form;
use App\Admin;

use Auth;

class AdminController extends Controller
{
   function index(){
		$user = Auth::guard('admin')->user();

		$new = Form::where('is_seen', 0)->count();
		$total = Form::whereNull('deleted_at')->count();

    	return view('Admin.index', compact('new', 'total'));
	}

	function list_form(){
		$form = Form::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $form]);
	}

	function delete_form($form_id){
		$form = Form::find($form_id);
		$form->deleted_at = now();
		if($form->save()){
			return response()->json(['status' => true, 'message' => 'Form deleted successfully!']);
		}
	}
}
