<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Mail;
use App\Mail\AccountVerify;

use App\ForgotPassword;


use Validator;
use Str;

use App\Admin;
use App\User;

use Carbon\Carbon;

class TeacherController extends Controller
{
    function index(){
    	return view('Admin.teacher');
    }

     function if_url($param){
    	$result = (!empty($param))? 'url' : '';

    	return $result;
    }

    function is_unique($param,$tbl){
    	$result = ($param > 0) ? '|unique:'.$tbl : '';

    	return $result;
    }

    function send_mail($user){
    	  $token = Str::random(10);
		  $forgot = new ForgotPassword;
	      $forgot->forgotable_id = $user->id;
	      $forgot->forgotable_type = get_class($user);
	      $forgot->token = $token;
	      $forgot->date_validity = Carbon::now()->add(10, 'minutes');

	      if ($forgot->save()) {
	        $to_name = $user->name;
	        $to_email = $user->email;
	        $url_link = $url_link = url('/new_password/'.$token.'/'.$user->id.'');

	        $data = array('name' => $to_name, 'url_link' => $url_link);
	        Mail::to($to_email)->send(new AccountVerify($data));
	        return response()->json(['status' => true, 'message' => 'Please check your email. We will send your link.']);
	      }
    }

    function list(){
		$teacher = Admin::where('user_type', 3)->whereNull('date_deleted')->get();
		return response()->json(['status' => true, 'data' => $teacher]);
	}

	function add(Request $request){
		$id= $request->get('id');
		$name = $request->get('name');
		$username = $request->get('username');
		$email = $request->get('email');


		$username_check = Admin::where('username', $username)->whereNull('date_deleted')->where('id', '!=', $id)->count();
		$email_check = Admin::where('email', $email)->whereNull('date_deleted')->where('id', '!=', $id)->count();

		$username_check_user = User::where('username', $username)->whereNull('date_deleted')->where('user_id', '!=', $id)->count();
		$email_check_user = User::where('email', $email)->whereNull('date_deleted')->where('user_id', '!=', $id)->count();

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'username' => 'required'. $this->is_unique($username_check, 'sm_admin').$this->is_unique($username_check_user, 'sm_users'),
			'email' => 'required|email'. $this->is_unique($email_check, 'sm_admin').$this->is_unique($email_check_user, 'sm_users'),
		]);

		if ($validator->fails()) {
			return response()->json(['status' => false, 'error' => $validator->errors()]);
		}else{
			if (!empty($id)) {
				$teacher = Admin::find($id);
				$teacher->name = $name;
				$teacher->username = $username;
				$teacher->email = $email;
				if($teacher->save()){
					return response()->json(['status' => true, 'message' => 'Teacher updated successfully!']);
				}
			}else{
				$teacher = new Admin;
				$teacher->name = $name;
				$teacher->username = $username;
				$teacher->email = $email;
				// $form->verification_code = Str::random(20);
				$teacher->password = Str::random(10);
				if($teacher->save()){
					$this->send_mail($teacher);
					return response()->json(['status' => true, 'message' => 'Teacher saved successfully!']);
				}
			}
		}
	}


	function delete_teacher($id){
		$teacher = Admin::find($id);
		$teacher->date_deleted = now();

		if($teacher->save()){
			return response()->json(['status' => true, 'message' => 'Teacher deleted successfully!']);
		}
	}
}
