<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Email;

use Validator;
use Auth;
use Str;
use Hash;

class EmailController extends Controller
{
  function list_email(){
	$email = Email::whereNull('deleted_at')->get();
	return response()->json(['status' => true, 'data' => $email]);
}

function add_email(Request $request){
	$email_id= $request->get('email_id');
	$email_address = $request->get('email_address');

	$validator = Validator::make($request->all(), [
		'email_address' => 'required|email',
	]);

	if ($validator->fails()) {
		return response()->json(['status' => false, 'error' => $validator->errors()]);
	}else{
		if (!empty($email_id)) {
			$email = Email::find($email_id);
			$email->email_address = $email_address;
			if($email->save()){
				return response()->json(['status' => true, 'message' => 'Email updated successfully!']);
			}
		}else{
			$email = new Email;
			$email->email_address = $email_address;
			if($email->save()){
				return response()->json(['status' => true, 'message' => 'Email saved successfully!']);
			}
		}
	}
}


function delete_email($email_id){
	$email = Email::find($email_id);
	if($email->delete()){
		return response()->json(['status' => true, 'message' => 'Email deleted successfully!']);
	}
}
}
