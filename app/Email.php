<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Hash;


class Email extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $primaryKey = 'email_id';

    protected $table = 'bona_email_config';

    protected $fillable = ['email_address','created_at','updated_at','deleted_at'];

     public function getCreatedAtAttribute($value)
    {
        return  \Carbon\Carbon::parse($value)->format('jS F Y');
    }
}
