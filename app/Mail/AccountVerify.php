<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AccountVerify extends Mailable
{
    use Queueable, SerializesModels;
    
    public $data;


    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($data)
    {
        $this->data = $data;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('smtp@aiosw.com', 'Testing(Student Management)')->subject('Account Confirmation')->markdown('emails.account.verify');
        // return $this->markdown('emails.account.verify');
    }
}
