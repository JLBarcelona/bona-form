<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Student', 'icon' => asset('img/logophone.png') ])
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/exporting.js"></script>
	<script src="https://code.highcharts.com/modules/export-data.js"></script>
	<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<body class="font-base" onload="show_student();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						Student
						<button class="btn btn-primary btn-sm float-right" onclick="add_student();"><i class="fa fa-plus"></i> Add Student</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_student" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>

	@include('Layout.footer', ['type' => 'admin'])
</html>
<div class="modal fade" role="dialog" id="modal_add_student">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Add Student

            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
            <form class="needs-validation" id="add_form_student" action="{{ url('/student_add') }}" novalidate>
				<div class="form-row">
				<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control ">
				<div class="form-group col-sm-12">
						<label>Name </label>
						<input type="text" id="name" name="name" placeholder="Name" class="form-control " required>
						<div class="invalid-feedback" id="err_name"></div>
					</div>
				<div class="form-group col-sm-12">
						<label>Username </label>
						<input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
						<div class="invalid-feedback" id="err_username"></div>
					</div>
				<div class="form-group col-sm-12">
						<label>Email </label>
						<input type="email" id="email" name="email" placeholder="Email" class="form-control " required>
						<div class="invalid-feedback" id="err_email"></div>
					</div>

					<div class="col-sm-12 text-right">
						<button class="btn btn-dark btn-sm" id="btn_save" type="submit">Save</button>
					</div>
				</div>
			</form>
          </div>
          <div class="modal-footer">

          </div>
        </div>
      </div>
    </div>

     <div class="modal fade" role="dialog" id="modal_progress">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <div class="modal-title student_name">
                	Loading...
                </div>
                <button class="close" data-dismiss="modal">&times;</button>
              </div>
              <div class="modal-body">
	               <div class="row">
					<div class="col-sm-12">
						<div class="card">
							<div class="card-header"><span class="student_name"></span> Progress</div>
							<div class="card-body">
								<div class="container" id="container"></div>
							</div>
							<div class="card-footer"></div>
						</div>


						<div class="card mt-2">
							<div class="card-header">Report Details</div>
							<div class="card-body">
								<table class="table table-bordered" id="user_table_detail" style="width: 100%;"></table>
							</div>
							<div class="card-footer"></div>
						</div>
					</div>
				</div>
              </div>
              <div class="modal-footer">

              </div>
            </div>
          </div>
        </div>

<!-- Javascript Function-->
<script>

	var tbl_student;

	function show_student(){
			if (tbl_student) {
				tbl_student.destroy();
			}

			var url = main_path + '/student/list';
			tbl_student = $('#tbl_student').DataTable({
	        pageLength: 10,
	        responsive: true,
	        ajax: url,
	        deferRender: true,
				language: {
					 "emptyTable": "No data available"
				},
	        columns: [
	         {
	         	 className: '',
	            "data": "name",
	            "title": "Name",
	          },{
	         	 className: '',
	            "data": "username",
	            "title": "Username",
	          },{
	         	 className: '',
	            "data": "email",
	            "title": "Email",
	          },{
	            className: 'width-option-1 text-center',
	            "data": "user_id",
	            "orderable": false,
	            "title": "Options",
	            "render": function(data, type, row, meta){
	              var param_data = JSON.stringify(row);
	              // console.log();
	              // var data_param = JSON.parse(param_data);
	              	newdata = '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_student(this)" type="button"><i class="fa fa-edit"></i> Edit</button>\
							   <button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="delete_student(this)"><i class="fa fa-trash"></i> Delete</button>\
							   <button class="btn btn-info btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' type="button" onclick="show_progress(this)"><i class="fa fa-eye"></i> Progress</button>';

	              return newdata;
	            }
	          }
	        ]
	        });
		}

	function add_student(){
		clear_student_form();
		$("#modal_add_student").modal('show');
	}

	function clear_student_form(){
		$("#user_id").val('');
		$("#name").val('');
		$("#username").val('');
		$("#email").val('');
	}

	$("#add_form_student").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					loading();
				// swal("Loading...","Please Wait.. \n we will sent the confirmation link to your email", "info");
				$("#btn_save").attr('disabled', true);
			},
			success:function(response){
					$("#btn_save").attr('disabled', false);
				if(response.status == true){
					// console.log(response)
      		swal("Success", response.message, "success");
					showValidator(response.error,'add_form_student');
					show_student();
					clear_student_form();
					$("#modal_add_student").modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'add_form_student');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});
</script>

<script type="text/javascript">
function edit_student(_this){
	var data = JSON.parse($(_this).attr('data-info'));

	$("#user_id").val(data.user_id);
	$("#name").val(data.name);
	$("#username").val(data.username);
	$("#email").val(data.email);
	$("#modal_add_student").modal('show');

}


function delete_student(_this){
  var data = JSON.parse($(_this).attr('data-info'));
  var url = main_path + '/student/delete/' + data.user_id;
  swal({
        title: "Are you sure?",
        text: "Do you want to delete ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: false
      },
        function(){
         $.ajax({
          type:"GET",
          url:url,
          data:{},
          dataType:'json',
          beforeSend:function(){
          },
          success:function(response){
            // console.log(response);
           if (response.status == true) {
              swal("Success", response.message, "success");
              show_student();
           }else{
            console.log(response);
           }
          },
          error: function(error){
            console.log(error);
          }
        });
      });
  }
</script>



<script type="text/javascript">


	function show_progress(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var id = data.user_id;

		get_graph(id);
		show_user_table_detail(id)
		$("#modal_progress").modal('show');
	}

	function get_graph(id){

		var url = main_path + '/student/progress/' + id;

		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      // console.log(response);
		     if (response.status == true) {
		        // swal("Success", response.message, "success");
		        show_charts(response.data);
		     }else{
		      console.log(response);
		     }
		    },
		    error: function(error){
		      console.log(error);
		    }
		  });
	}

	function show_charts(datas){
	// Create the chart
	var data_info = JSON.parse(datas);
	var marker = (data_info.length > 0) ? data_info[0].pass_marker : 0;

	// console.log(datas);
	Highcharts.chart('container', {
	  chart: {
	    type: 'column'
	  },
	  title: {
	    text: 'Course Statistics'
	  },
	  subtitle: {
	    text: ''
	  },
	  accessibility: {
	    announceNewData: {
	      enabled: true
	    }
	  },
	  xAxis: {
	    type: 'category'
	  },
	  yAxis: {
	    title: {
	      text: 'Total Percentage'
	    }

	  },
	  legend: {
	    enabled: false
	  },
	  plotOptions: {
	    series: {
	      borderWidth: 0,
	      dataLabels: {
	        enabled: true,
	        format: '{point.y:.1f}%'
	      }
	    }
	  },

	  tooltip: {
	    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
	    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
	  },
	  series: [
	    {
	      name: "",
	      colorByPoint: false,
	      data: JSON.parse(datas)
	    }
	  ]
	},function(chart){

        $.each(chart.series[0].data,function(i,data){
		  var max = data.pass_marker;

			console.log(max);
			console.log(data.y);
            if(data.y >= max){
				data.update({
						color:'#1cc88a'
					});
			}else{
				data.update({
						color:'#e74a3b'
					});
			}
        });

    });
}
</script>

<script type="text/javascript">
	var user_table_detail;

	function show_user_table_detail(id){
		if (user_table_detail) {
			user_table_detail.destroy();
		}
		var url = main_path + '/student/progress/table/' + id;
		user_table_detail = $('#user_table_detail').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "name",
		"title": "Name",
	},{
		className: '',
		"data": "score",
		"title": "Score",
	},{
		className: '',
		"data": "y",
		"title": "Percentage",
	},{
		className: '',
		"data": "passing_mark",
		"title": "Passing mark",
	},{
		className: '',
		"data": "pass_marker",
		"title": "Percentage",
	},{
		className: '',
		"data": "total_points",
		"title": "Total Question",
	},{
		className: 'width-option-1 text-center',
		"data": "purchased_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				// var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<a target="_blank" href="'+url_path('/student/exam/'+row.purchased_id )+'" class="btn btn-info btn-sm font-base mt-1" ><i class="fa fa-search"></i> View</a>';
				return newdata;
			}
		}
	]
	});
	}
</script>
