<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Teacher', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_teacher();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Teacher
					<button class="btn btn-primary btn-sm float-right" onclick="add_teacher();"><i class="fa fa-plus"></i> Add Teacher</button>
					</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_teacher" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>

<div class="modal fade" role="dialog" id="modal_add_teacher">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Add Teacher
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <form class="needs-validation" id="teacher_add_form" action="{{ url('teacher/add') }}" novalidate>
			<div class="form-row">
				<input type="hidden" id="id" name="id" placeholder="" class="form-control" required>
				<div class="form-group col-sm-12">
					<label>Name </label>
					<input type="text" id="name" name="name" placeholder="Name" class="form-control " required>
					<div class="invalid-feedback" id="err_name"></div>
				</div>
				<div class="form-group col-sm-12">
					<label>Username </label>
					<input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
					<div class="invalid-feedback" id="err_username"></div>
				</div>
				<div class="form-group col-sm-12">
					<label>Email </label>
					<input type="email" id="email" name="email" placeholder="Email" class="form-control " required>
					<div class="invalid-feedback" id="err_email"></div>
				</div>

				<div class="col-sm-12 text-right">
					<button class="btn btn-dark btn-sm" id="btn_save" type="submit">Save</button>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>



<script>

	function add_teacher(){
		clear_teacher_form();
		$("#modal_add_teacher").modal('show');
	}

	var tbl_teacher;
	function show_teacher(){
		if (tbl_teacher) {
			tbl_teacher.destroy();
		}
		var url = main_path + '/teacher/list';
		tbl_teacher = $('#tbl_teacher').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "name",
		"title": "Name",
	},{
		className: '',
		"data": "username",
		"title": "Username",
	},{
		className: '',
		"data": "email",
		"title": "Email",
	},{
		className: 'width-option-1 text-center',
		"data": "id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				var param_data = JSON.stringify(row);
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="edit_teacher(this)" type="button"><i class="fa fa-edit"></i> Edit</button> ';
				newdata += '<button class="btn btn-danger btn-sm font-base mt-1" data-info=\''+param_data.trim()+'\' onclick="delete_teacher(this)" type="button"><i class="fa fa-edit"></i> Delete</button> ';
				return newdata;
			}
		}
	]
	});
	}

	$("#teacher_add_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					loading();
					$("#btn_save").attr('disabled', true);
			},
			success:function(response){
				$("#btn_save").attr('disabled', false);
				if(response.status == true){
					console.log(response);
					swal("Success", response.message, "success");
					show_teacher();
					clear_teacher_form();
					$("#modal_add_teacher").modal('hide');
					showValidator(response.error,'teacher_add_form');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'teacher_add_form');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_teacher(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		var url =  main_path + '/teacher/delete/' + data.id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this teacher?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"GET",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_teacher();

				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function clear_teacher_form(){
		$("#id").val('');
		$('#name').val('');
		$('#username').val('');
		$('#email').val('');
	}

	function edit_teacher(_this){
		var data = JSON.parse($(_this).attr('data-info'));
		// console.log(data.name);

		$("#id").val(data.id);
		$('#name').val(data.name);
		$('#username').val(data.username);
		$('#email').val(data.email);
		$("#modal_add_teacher").modal('show');
	}
</script>
