<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'Admin', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_report(); show_app();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
	<div class="row">
			<div class="col-sm-3 mb-2">
				<div class="card border-primary">
					<div class="card-body bg-primary text-white">
						  <h6 class="card-title"><i class="fa fa-users"></i> WeChat Account  <span class="card-text float-right">{{ $total }}</span></h6>
					</div>
					<!-- <a href="{{ url('admin/student') }}" >
						<div class="card-footer">
							View More
						</div>
					</a> -->
				</div>
			</div>

			<div class="col-sm-3 mb-2">
				<div class="card border-success">
					<div class="card-body bg-success text-white">
						  <h6 class="card-title"><i class="fa fa-folder-open"></i> New WeChat Account  <span class="card-text float-right">{{ $new }}</span></h6>
					</div>
				<!-- 	<a href="{{ url('admin/student') }}" >
						<div class="card-footer">
							View More
						</div>
					</a> -->
				</div>
			</div>

			<div class="col-sm-3 mb-2">
				<div class="card border-info">
					<div class="card-body bg-info text-white">
						  <h6 class="card-title"><i class="fa fa-users"></i> WeChat Application  <span class="card-text float-right">{{ $total }}</span></h6>
					</div>
					<!-- <a href="{{ url('admin/student') }}" >
						<div class="card-footer">
							View More
						</div>
					</a> -->
				</div>
			</div>

			<div class="col-sm-3 mb-2">
				<div class="card border-danger">
					<div class="card-body bg-danger text-white">
						  <h6 class="card-title"><i class="fa fa-folder-open"></i> New WeChat Application  <span class="card-text float-right">{{ $new }}</span></h6>
					</div>
				<!-- 	<a href="{{ url('admin/student') }}" >
						<div class="card-footer">
							View More
						</div>
					</a> -->
				</div>
			</div>
		</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="card mt-3 mb-4">
				<div class="card-header bg-primary text-white">We Chat Account Report</div>
				<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_report" style="width: 100%;"></table>
				</div>
				<div class="card-footer"></div>
			</div>

			<div class="card mt-3">
				<div class="card-header bg-info text-white">We Chat Application Report</div>
				<div class="card-body">
					<table class="table table-bordered dt-responsive nowrap" id="tbl_app" style="width: 100%;"></table>

				</div>
				<div class="card-footer"></div>
			</div>
		</div>
	</div>
</div>

</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>


<script type="text/javascript">
var tbl_report;
function show_report(){
	if (tbl_report) {
		tbl_report.destroy();
	}
	var url = main_path + '/form/list_form';
	tbl_report = $('#tbl_report').DataTable({
	pageLength: 10,
	responsive: true,
	ajax: url,
	deferRender: true,
	language: {
	"emptyTable": "No data available"
},
	columns: [{
	className: '',
	"data": "account_name",
	"title": "Account Name",
},{
	className: '',
	"data": "customer_service_phone",
	"title": "Customer Service Phone",
},{
	className: '',
	"data": "created_at",
	"title": "Date Submit",
},{
	className: 'text-center',
	"data": "status",
	"title": "Status",
}
]
});
}


var tbl_app;
function show_app(){
  if (tbl_app) {
    tbl_app.destroy();
  }
  var url = main_path + '/app/list_app';
  tbl_app = $('#tbl_app').DataTable({
  pageLength: 10,
  responsive: true,
  ajax: url,
  deferRender: true,
  language: {
  "emptyTable": "No data available"
},
  columns: [{
  className: '',
  "data": "email_address",
  "title": "Email_address",
},{
  className: '',
  "data": "company_name",
  "title": "Company_name",
},{
  className: '',
  "data": "company_registration_number",
  "title": "Company_registration_number",
},{
  className: '',
  "data": "company_registration_address",
  "title": "Company_registration_address",
},{
  className: '',
  "data": "company_website",
  "title": "Company_website",
},{
	className: 'text-center',
	"data": "status",
	"title": "Status",
}
]
});
}

</script>
