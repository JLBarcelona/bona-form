<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'admin', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_email()">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">Email Notification Settings <button type="button" name="button" class="btn float-right btn-sm btn-primary" onclick="add_email()">Add Email</button>	</div>
					<div class="card-body">
						<table class="table table-bordered dt-responsive nowrap" id="tbl_email" style="width: 100%;"></table>
					</div>
					<div class="card-footer"></div>
				</div>
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])

<div class="modal fade" role="dialog" id="modal_add_email">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Add Email
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
				<form class="needs-validation" id="email_form_id" action="{{ url('email/add_email') }}" novalidate>
					<div class="form-row">
						<input type="hidden" id="email_id" name="email_id" placeholder="" class="form-control" required>
						<div class="form-group col-sm-12">
							<label>Email Address </label>
							<input type="email" id="email_address" name="email_address" placeholder="Email Address" class="form-control " required>
							<div class="invalid-feedback" id="err_email_address"></div>
						</div>

						<div class="col-sm-12 text-right">
							<button class="btn btn-dark btn-sm" type="submit">Save</button>
						</div>
					</div>
				</form>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>

</html>
<script type="text/javascript">
	function add_email(){
		$('#email_id').val('');
		$('#email_address').val('');
		$("#modal_add_email").modal("show");
	}

	var tbl_email;
function show_email(){
	if (tbl_email) {
		tbl_email.destroy();
	}
	var url = main_path + '/email/list_email';
	tbl_email = $('#tbl_email').DataTable({
	pageLength: 10,
	responsive: true,
	ajax: url,
	deferRender: true,
	language: {
	"emptyTable": "No data available"
},
	columns: [{
	className: '',
	"data": "email_address",
	"title": "Email_address",
},{
	className: 'width-option-1 text-center',
	"data": "email_id",
	"orderable": false,
	"title": "Options",
		"render": function(data, type, row, meta){
			var param_data = JSON.stringify(row);
			newdata = '';
			newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="edit_email(this)" type="button"><i class="fa fa-edit"></i> Edit</button>';
			newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_email(this)" type="button"><i class="fa fa-edit"></i> delete</button>';
			return newdata;
		}
	}
]
});
}

$("#email_form_id").on('submit', function(e){
	var url = $(this).attr('action');
	var mydata = $(this).serialize();
	e.stopPropagation();
	e.preventDefault(e);

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
			loading();
				//<!-- your before success function -->
		},
		success:function(response){
				//console.log(response)
			if(response.status == true){
				console.log(response)
				swal("Success", response.message, "success");
				showValidator(response.error,'email_form_id');
				show_email();
				$('#email_id').val('');
				$('#email_address').val('');
				$("#modal_add_email").modal("hide");
			}else{
				//<!-- your error message or action here! -->
				showValidator(response.error,'email_form_id');
			}
		},
		error:function(error){
			console.log(error)
		}
	});
});

function delete_email(_this){
	var data = JSON.parse($(_this).attr('data-info'));
	var url =  main_path + '/email/delete_email/' + data.email_id;
		swal({
			title: "Are you sure?",
			text: "Do you want to delete this email?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
			type:"GET",
			url:url,
			data:{},
			dataType:'json',
			beforeSend:function(){
				loading();
		},
		success:function(response){
			console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_email();
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

function edit_email(_this){
	var data = JSON.parse($(_this).attr('data-info'));
	$('#email_id').val(data.email_id);
	$('#email_address').val(data.email_address);
	$("#modal_add_email").modal("show");
}
</script>
