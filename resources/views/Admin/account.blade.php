<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'admin', 'title' => 'WeChat Application', 'icon' => asset('img/logophone.png') ])

<body class="font-base" onload="show_form();">
	@include('Layout.nav', ['type' => 'admin'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-12">
        <div class="card">
        	<div class="card-header">WeChat Account</div>
        	<div class="card-body">
            <table class="table table-bordered dt-responsive nowrap" id="tbl_form" style="width: 100%;"></table>
        	</div>
        	<div class="card-footer"></div>
        </div>
      </div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'admin'])
</html>
<script type="text/javascript">
var tbl_form;
function show_form(){
  if (tbl_form) {
    tbl_form.destroy();
  }
  var url = main_path + '/form/list_form';
  tbl_form = $('#tbl_form').DataTable({
  pageLength: 10,
  responsive: true,
  ajax: url,
  deferRender: true,
  language: {
  "emptyTable": "No data available"
},
  columns: [{
  className: '',
  "data": "account_name",
  "title": "Account Name",
},{
  className: '',
  "data": "customer_service_phone",
  "title": "Customer Service Phone",
},{
  className: '',
  "data": "created_at",
  "title": "Date Submit",
},{
  className: 'text-center',
  "data": "status",
  "title": "Status",
},{
  className: 'width-option-1 text-center',
  "data": "form_id",
  "orderable": false,
  "title": "Options",
    "render": function(data, type, row, meta){
      var param_data = JSON.stringify(row);
      newdata = '';
      newdata += '<a target="_blank" class="btn btn-info btn-sm font-base mt-1" href="'+url_path('/form/view/'+row.form_id)+'"><i class="fa fa-eye"></i> View</a>';
      newdata += ' <a target="_blank" class="btn btn-success btn-sm font-base mt-1" href="'+url_path('/form/edit/'+row.form_id)+'"><i class="fa fa-edit"></i> Edit</a>';
      newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-info=\' '+param_data.trim()+'\' onclick="delete_form(this)" type="button"><i class="fa fa-edit"></i> Delete</button>';
      return newdata;
    }
  }
]
});
}

function delete_form(_this){
  var data = JSON.parse($(_this).attr('data-info'));
  var url =  main_path + '/form/delete_form/' + data.form_id;
    swal({
      title: "Are you sure?",
      text: "Do you want to delete this form?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes",
      closeOnConfirm: false
    },
    function(){
      $.ajax({
      type:"GET",
      url:url,
      data:{},
      dataType:'json',
      beforeSend:function(){
    },
    success:function(response){
      // console.log(response);
      if (response.status == true) {
        swal("Success", response.message, "success");
        show_form();
      }else{
        console.log(response);
      }
    },
    error: function(error){
      console.log(error);
    }
    });
  });
}
</script>
