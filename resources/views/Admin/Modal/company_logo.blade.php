<div class="modal fade" role="dialog" id="modal_upload_image">

    <div class="modal-dialog modal-dialog-centered" style="max-width: auto !important; max-height: auto !important;">

      <div class="modal-content" style="max-width: auto !important; max-height: auto !important;">

        <div class="modal-header">

          <div class="modal-title">

          Crop

          </div>

          <button class="close" data-dismiss="modal">&times;</button>

        </div>

        <div class="modal-body text-center" style="max-width: auto !important; max-height: auto !important;">

           <div id="upload-demo" style="width:450px"></div>
           <div class="row">
             <div class="col-sm-2"></div>
             <div class="col-sm-4 form-group">
              <label>Select Visible</label>
              <input type="checkbox" name="is_full_width" id="is_full_width">
             </div>

<!--              <div class="col-sm-4 form-group">
              <label>Resize</label>
              <input type="checkbox" name="auto_size" id="auto_size" oninput="show_croppie();">
             </div>
 -->
             <div class="col-sm-4 form-group">
              <label>Circle</label>
              <input type="checkbox" name="is_circle" id="is_circle" oninput="show_croppie();">
             </div>

             <div class="col-sm-2"></div>

           </div>
           <button class="btn btn-success upload-result">Upload Image</button>

        </div>

        <div class="modal-footer">
        </div>

      </div>

    </div>

  </div>



<script type="text/javascript">

  var width = '';
  var height = '';
  var $uploadCrop;
  var urls = '';
  initialCrop('');

  function initialCrop(val,shape){
      // console.log(val);
      $uploadCrop = $('#upload-demo').croppie({
        enableExif: true,
        viewport: {

            width: 150,

            height: 150,

            type: shape

        },
        boundary: {

            width: 300,

            height: 300

        },
        showZoomer: true,
        enableResize: val,
        enableOrientation: true,
        enforceBoundary: false
    });


    // alert(height+' '+width);
    return($uploadCrop);

  }



  $("#is_full_width").on('click', function(){
     if($('#is_full_width').is(":checked")){
        width = $(".cr-image").attr('width');
        height = $(".cr-image").attr('height');

        $(".cr-viewport").attr('style','height:'+height+'; width:'+width+'; ');
        $(".cr-viewport").addClass('border border-dark');
        $(".cr-slider").hide();
        // alert(height +' '+ width);
      }
      else if($('#is_full_width').is(":not(:checked)")){
        show_croppie();
        $(".cr-slider").show();
        $(".cr-viewport").removeClass('border border-dark');
      }
  });

  function show_croppie(){
     var check;
     var circle;

      if($('#auto_size').is(":checked")){
          check = true;
        }
      else if($('#auto_size').is(":not(:checked)")){
          check = false;
      }

       if($('#is_circle').is(":checked")){
          circle = 'circle';
        }
      else if($('#is_circle').is(":not(:checked)")){
          circle = 'square';
      }


     $uploadCrop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
      }).then(function (resp) {
          reset_image($uploadCrop);
          $uploadCrop = initialCrop(check, circle);
          $('.upload').removeClass("img-fluid mx-auto d-block");
             $uploadCrop.croppie('bind', {
                url: urls
              }).then(function(){
            });
      });



  }


function reset_image($uploadCrop) {
    $uploadCrop ? $uploadCrop.croppie('destroy') : '';

    $('.upload').removeClass("cr-original-image").addClass("img-fluid mx-auto d-block");
    $(".image_overlay .view.overlay").prepend($(".image_overlay .view.overlay > div").html());
    $(".image_overlay .view.overlay > div").remove();
}


  $('#upload_pict').on('change', function () {

    var reader = new FileReader();

      reader.onload = function (e) {

        if (e.total/1024/1024 > 1.99999999999999) {
          swal("Oops!","File is too big!","error");
        }else{
            $("#add_company").modal('hide');
            $("#modal_upload_image").modal('show');
            urls = e.target.result;
            // console.log(e.total/1024/1024);
            $uploadCrop.croppie('bind', {
              url: e.target.result
            }).then(function(){
              // console.log('jQuery bind complete');
            });
        }

      }

      reader.readAsDataURL(this.files[0]);

  });





  $('.upload-result').on('click', function (ev) {

    // var url ='/user4/CRM/public/image-crop'; //LIVE
    var url = main_path + '/image-crop'; //LOCAL

    $uploadCrop.croppie('result', {

      type: 'canvas',

      size: 'viewport'

    }).then(function (resp) {

      $.ajax({

        url: url,

        type: "POST",

        data: {"image":resp},

        dataType:'json',

        success: function (response) {

          $("#add_company").modal('show');

          $("#modal_upload_image").modal('hide');



          html = '<img src="' + resp + '" class="img-fluid animated jackInTheBox" width="110" />';

          $("#upload-demo-i").html(html);

          $("#company_logo").val(response.name);
          remove_all_validation();


        }

      });

    });

  });

</script>
