<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Laravel')
<img src="{{ asset('img/logo.jpg') }}" width="120" alt="Student Management">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
