<!-- Common -->
<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/2.4.4/umd/popper.min.js"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/ajaxsetup.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/sweetalert.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/index/index.js') }}"></script> -->
@if($type == 'home')

@elseif($type == 'admin')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>
@elseif($type == 'teacher')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>
@elseif($type == 'student')
<script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowdata.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/rowresponsive.js') }}"></script>
<script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.min.js') }}"></script>
@endif