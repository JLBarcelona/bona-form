<style type="text/css">
  .pointer{
    cursor: pointer;
  }
  .no-highlights{
    user-select: none;
  }
</style>

<div class="modal fade no-highlights" role="dialog" id="user_guide_form">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        User Guide
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <div class="accordion" id="accordionExample">
            <div class="card">
              <div class="card-header bg-primary text-white pointer" id="headingOne"  data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                <h5 class="mb-0">
                   How to create form?
                </h5>
              </div>

              <div id="collapseOne" class="collapse collapsed" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><!-- <i class="fa fa-list-alt"></i> --> Forms</b>.</p>
                  <p>2. Click the add button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Form</span>.</p>
                  <p>3. Enter the <b>Form name</b>, Form <b>Passing Mark</b> and <b>Timer</b>.</p>
                  <p>4. Click the Save button <span class="p-2 bg-dark rounded text-white">Save</span>.</p>

                  <hr>

                  <h5><strong>How to add category inside the form?</strong></h5>
                  <br>
                  <div class="pl-2">
                    <p>1. Click the <b>Add Category</b> button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Category</span> form will show.</p>

                    <p>2. Select the <b>Category</b> you want to add. <br> <small><span>If category choices is empty, you can add category <a href="{{ url('/admin/category') }}">here</a>.</span></small></p>


                    <p>3. Enter the <b>Number of question </b>. <br><small>(How many question will be generated for this category inside the form.)</small></p>

                    <p>4. Click the Save button <span class="p-2 bg-dard rounded text-white">Save</span>.</p>

                  <!--   <div class="">
                      <p><strong class="h5">Note:</strong> <small>You can <b>Edit</b> and <b>Remove</b> the category using the option button <span class="p-1 btn-circle bg-info text-white"><i class="fa fa-cog"></i></span> Before the category name.</small></p>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header bg-primary text-white pointer collapsed" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                <h5 class="mb-0">
                   How to create category?
                </h5>
              </div>
              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><!-- <i class="fa fa-list"></i> --> Category</b>.</p>
                  <p>2. Click the add button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Category</span>.</p>
                  <p>3. Enter the <b>Category name</b>.</p>
                  <p>4. Click the Save button <span class="p-2 bg-dark rounded text-white">Save</span>.</p>

                   <hr>

                  <h5><strong>How to add question inside the category?</strong></h5>
                  <br>
                  <div class="pl-2">
                    <p>1. Click the <b>Add Question button</b> <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus"></i> Add Question</span> form will show.</p>

                    <p>2. Write the <b>Question</b>.</p>

                    <p>3. Click the Save button <span class="p-2 bg-dark rounded text-white">Save</span>.</p>
                    <hr>
                    <h5> <strong>How to add choices for question ?</strong></h5>
                    <br>
                    <div class="pl-2">
                      <p>1. Click the <b>Answer Choices Option</b> tab.</p>
                      <!-- <p>2. Enter the choices <b>Sorting</b> <small>(Example: a,b,c)</small>.</p> -->
                      <p>2. Enter the choice <b>Answer</b>.</p>
                      <p>3. Enter the <b>Choice Points</b>.</p>
                      <p>4. Click the save button <span class="p-2 bg-dark rounded text-white">Save</span>.

                    </div>

                  </div>

                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header bg-primary text-white pointer collapsed" id="headingThree" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <h5 class="mb-0">
                     How to create product?
                </h5>
              </div>
              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><!-- <i class="fa fa-tag"></i> --> Product Inventory</b>.</p>
                  <p>2. Click the add button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Product</span>.</p>
                  <p>3. Choose the <b>Form</b> you want to become a product.</p>
                  <p>4. Enter the product <b>Price</b>.</p>
                  <!-- <p><strong>Note:</strong> You can also add <b>Sale Price</b> it's optional.</p>
                  <p>5. Click the submit button <span class="p-2 bg-info rounded text-white">Submit</span>.</p> -->
                </div>
              </div>
            </div>

             <div class="card">
              <div class="card-header bg-primary text-white pointer collapsed" id="headingFour" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                <h5 class="mb-0">
                     How to add student ?
                </h5>
              </div>
              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><!-- <i class="fa fa-users"></i> --> Students</b>.</p>
                  <p>2. Click the add button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Student</span>.</p>
                  <p>3. Fill up all the student information.</p>
                  <p>4. Click the save button <span class="p-2 bg-dark rounded text-white">Save</span>.</p>
                  <p>5. The student will receive an email account confirmation.</p>
                </div>
              </div>
            </div>


               <div class="card">
              <div class="card-header bg-primary text-white pointer collapsed" id="teacher" data-toggle="collapse" data-target="#collapseteacher" aria-expanded="false" aria-controls="collapseteacher">
                <h5 class="mb-0">
                     How to add teacher ?
                </h5>
              </div>
              <div id="collapseteacher" class="collapse" aria-labelledby="teacher" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><!-- <i class="fa fa-chalkboard-teacher"></i> --> teacher</b>.</p>
                  <p>2. Click the add button <span class="p-2 bg-primary rounded text-white"><i class="fa fa-plus-circle"></i> Add Teacher.</p>
                  <p>3. Fill up all the teacher information.</p>
                  <p>4. Click the save button <span class="p-2 bg-dark rounded text-white">Save</span>.</p>
                  <p>5. The teacher will receive an email account confirmation.</p>
                </div>
              </div>
            </div>


            <!--   <div class="card">
              <div class="card-header bg-primary text-white pointer collapsed" id="sub-category" data-toggle="collapse" data-target="#collapseSub" aria-expanded="false" aria-controls="collapseSub">
                <h5 class="mb-0">
                     How to add sub-category ?
                </h5>
              </div>
              <div id="collapseSub" class="collapse" aria-labelledby="sub-category" data-parent="#accordionExample">
                <div class="card-body">
                  <p>1. Go to <b><i class="fa fa-list"></i> category</b>.</p>
                  <p>2. Click the option button <span class="p-2 bg-dark rounded text-white"><i class="fa fa-cog"></i></span>.</p>
                  <p>3. Click <i class="fa fa-edit"></i> edit.</p>
                  <p>4. Click Add sub category button <span class="p-2 bg-dark rounded text-white"><i class="fa fa-plus"></i>Add sub category</span>.</p>
                  <p>5. Fill up all the sub-category information.</p>
                  <p>6. Click save <span class="p-2 bg-info rounded text-white">save</span>.</p>
                </div>
              </div>
            </div> -->

          </div>
        </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
