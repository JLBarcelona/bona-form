<!DOCTYPE html>
<style type="text/css">
	.bg{
		background-position: center;
		background-size: cover;
	}
	.card-form{
		text-align: center;
		margin-top: 10px;
	    max-height: 22.440528634361232vw;
	    max-width: 90vw;
	    height: 159.5770925110132px;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-form-base{
		margin-top: 10px;
	    max-width: 90vw;
	    height: auto;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-1{
		background-image: url(https://lh5.googleusercontent.com/zdX8VsNyWBRMdQps8rr9Abf0qbPwAILvbm8tJ3URtqJQHczIpIZ0Qi-1dLuSK-XPhjz3_JBTl4mxSuEtTM0o9YG2XdaZnNyfyIeFnqzKjKXqun2KUtQSitH1gAm4=w1135);
	}
	.buttons-group{
		padding-left: 17px !important;
	}
	input {
	  outline: 0;
	  border-width: 0 0 2px;
	  border-color: #c1c1c1;
	  transition: border-color 0.3s;
	}
	input:focus {
	  border-color: #a1a1a1;
	}
	.show{
		display: '';
	}
	.hide{
		display: none;
	}
  .full{
    width: 100%;
  }
  .font-base-sm{
    font-size: 13px;
  }
</style>

<style media="screen">
.label-info{
	background: #563d7c !important;
	color: white !important;
	padding: 3px;
	border-radius: 5px;
	margin: 5px;
	}
.bootstrap-tagsinput{
	margin-top: 10px;
	width: 100% !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
}
.bootstrap-tagsinput>input{
	margin-top: 10px !important;
	width: 400px !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;

}
</style>
<!-- https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135 -->
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha512-wu4jn1tktzX0SHl5qNLDtx1uRPSj+pm9dDgqsrYUS16AqwzfdEmh1JR8IQL7h+phL/EAHpbBkISl5HXiZqxBlQ==" crossorigin="anonymous" />

<body class="font-base" style="background-color: #ebebeb;">
	<form class="needs-validation" id="form_add_form" action="{{ url('/application/add_form') }}" novalidate>
		<input type="hidden" value="5" name="submit" id="submit">
		<input type="hidden" value="2" name="page" id="page">
	<center>
	<div class="container-fluid mt-2 mb-3">
		<div class="row phase-1 hide">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">微信公众帐号注册及认证申请表 (服务号)</div>
								<p>WECHAT OFFICIAL ACCOUNT - REGISTRATION AND VERIFICATION APPLICATION FORM (SERVICE ACCOUNT)</p>
								<input type="text" placeholder="Email Address" style="width: 100%;" name="email_address" id="email_address">
								<div class="invalid-feedback" id="err_email_address"></div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm"><b>Next</b></button>
							</div>
						</div>
					</div>

				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>


    <!-- 2 -->
    <div class="row phase-2">
  			<div class="col-sm-3 col-lg-3 col-md-3"></div>
  			<div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">企业基本资料 COMPANY INFORMATION</div>
                <div class="card-body">
                  <p>需与当地政府颁发的企业注册证或商业登记证书等企业证件上的企业名称保持完全一致，信息审核成功后，企业名称不可修改。</p>
                  <p>THE COMPANY NAME MUST BE COMPLETELY CONSISTENT WITH THE COMPANY REGISTRATION CERTIFICATE ISSUED BY THE LOCAL GOVERNMENT. AFTER THE INFORMATION IS SUCCESSFULLY APPROVED, THE COMPANY NAME CANNOT BE MODIFIED.</p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业名称 :</div>
                  <p>COMPANY NAME (ALL CAPITAL) :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_name" id="company_name" class="full" onkeyup="this.value = this.value.toUpperCase();">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册号码 :</div>
                  <p>COMPANY REGISTRATION NUMBER :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" onkeypress="return num_only(event);" name="company_registration_number" id="company_registration_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册地址 :</div>
                  <p>COMPANY REGISTRATION ADDRESS :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_registration_address" id="company_registration_address" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业网站：</div>
                  <p>COMPANY WEBSITE :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_website" id="company_website" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <!-- <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-1', 'phase-2', 'phase-3', 'phase-4', 'phase-5', 'phase-6'); $('#submit').val(0);" type="button"><b>Back</b></button> -->
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-3', 'phase-1', 'phase-2', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
  	</div>

    <!-- 3 -->
    <div class="row phase-3 hide">
  			<div class="col-sm-3 col-lg-3 col-md-3"></div>
  			<div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">管理员信息登记</div>
                <div class="card-body">
                  <p>INFORMATION OF THE OFFICIAL ACCOUNT OPERATOR ("OA OPERATOR")</p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">

                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img src="https://lh5.googleusercontent.com/1XSiy2KvsvKzK_D4vDusAdVCsVIsgtdShFJ2fu4CXjWC531Mq3YgLji_nUy4WF7yql_vDHN1-Auv75DgvuESq12-TgtXiLoyrBy1jOFQTuO8jUoeDK85j1yH4kSA=w1236" alt="phone" class="img-fluid">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员姓名("帳號管理员") :</div>
                  <p>FULL NAME OF THE OFFICIAL ACCOUNT OPERATOR ("OA OPERATOR") :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="oa_operator" id="oa_operator" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员的护照号码及护照签发国 :</div>
                  <p>OA OPERATOR'S PASSPORT NUMBER AND ISSUING COUNTRY :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="oa_operator_passport_number" id="oa_operator_passport_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员手机号 :</div>
                  <p>MOBILE PHONE NUMBER OF OA OPERATOR :</p>
                  <div class="">
                    <input type="text" onkeypress="return num_only(event)" placeholder="Your Answer" name="oa_operator_mobile_number" id="oa_operator_mobile_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员座机: (座机用于联繫不到您时使用)</div>
                  <p>LANDLINE OF OA OPERATOR (THE LANDLINE IS USED WHEN WE CAN'T REACH YOU)</p>
                  <div class="">
                    <input type="text" onkeypress="return num_only(event)" placeholder="Your Answer" name="oa_landline" id="oa_landline" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="$('#submit').val(0); next('phase-2', 'phase-1', 'phase-3', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-4', 'phase-1', 'phase-2', 'phase-3', 'phase-5', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
  	</div>

    <!-- 4 -->
    <div class="row phase-4 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">微信帐号资料 WECHAT OFFICIAL ACCOUNT INFORMATION</div>
                <div class="card-body">
                  <p class="m-0">微信认证帐号命名规则:</p>
                  <p class="m-0">保护注册商标原则：帐号名不得侵犯注册商标专用权，否则审核将不被通过或面临法律诉讼；</p>
                  <p class="m-0">认证命名唯一原则：帐号名不得与已认证帐号重复，否则审核将不被通过。</p>
                  <br>
                  <p class="m-0">命名方式</p>
                  <p class="m-0">基于自选词汇命名  基于商标命名</p>
                  <br>
                  <p class="font-base-sm m-0">NAME RULES OF THE WECHAT OFFICIAL ACCOUNT :</p>
                  <p class="font-base-sm m-0">THE PRINCIPLE OF PROTECTING REGISTERED TRADEMARKS : ACCOUNT NAMES MUST NOT INFRINGE </p>
                  <p class="font-base-sm m-0">ON THE EXCLUSIVE RIGHTS OF REGISTERED TRADEMARKS, OTHERWISE THE AUDIT WILL NOT BE  </p>
                  <p class="font-base-sm m-0">APPROVED OR LEGAL PROCEEDINGS WILL BE FACED; </p>
                  <p class="font-base-sm m-0">THE ONLY PRINCIPLE OF CERTIFICATION NAME: THE ACCOUNT NAME MUST NOT BE THE SAME AS THE  </p>
                  <p class="font-base-sm m-0">CERTIFIED ACCOUNT, OTHERWISE THE AUDIT WILL NOT BE PASSED. </p>
                  <br>
                  <p class="font-base-sm m-0">NAME METHOD: </p>
                  <p class="font-base-sm m-0">NAME BASED ON SELF-SELECTED VOCABULARY OR NAME BASED ON TRADEMARK</p>

                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <!-- <div class="h6">请按照优先顺序，(30/30, 4~30个字符)（1个汉字算2个字符）提供至少三个微信公众帐号名称</div> -->
									<div class="h6">请按照优先顺序，(1 Chinese character = 2 English alphabets)<br> 提供至少三个微信公众帐号名称</div>

                  <p>PLEASE PROVIDE AT LEAST 3 WECHAT OFFICIAL ACCOUNT NAMES ACCORDING TO PRIORITY</p>
                  <div class="">
										<div class="mb-4">
											<input type="text" placeholder="Option 1" name="wechat_officials_account_names" id="wechat_officials_account_names" class="full">
										</div>

										<div class="mb-4">
											<input type="text" placeholder="Option 2" name="wechat_officials_account_names1" id="wechat_officials_account_names1" class="full">
										</div>

										<div class="mb-4">
											<input type="text" placeholder="Option 3" name="wechat_officials_account_names2" id="wechat_officials_account_names2" class="full">
										</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号功能介绍 ( 建议在120 个字内) :(指企业服务项目介绍)</div>
                  <!-- <p>ACCOUNT INTRODUCTION (4-120 WORDS) (COMPANY SERVICE INTRODUCTION)</p> -->
									<!-- <p>ACCOUNT INTRODUCTION (60 Chinese characters; 120 English alphabets) (COMPANY SERVICE INTRODUCTION)</p> -->
									<p>COMPANY INTRODUCTION & IT'S SERVICES (RECOMMENDED NOT MORE THAN 120 WORDS)</p>

                  <div class="">
                    <input type="text" placeholder="Your Answer" name="account_introduction" id="account_introduction" class="full">
                  </div>
									<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('document');"><i class="fa fa-upload"></i> Add Document</button>

									<textarea class="hide" name="document" id="document"></textarea>
									<div class="">
										<div id="document_preview"></div>
									</div>
                </div>
              </div>
            </div>



            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="$('#submit').val(0); next('phase-3', 'phase-1', 'phase-2', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-5', 'phase-1', 'phase-2', 'phase-3', 'phase-4', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div>


    <!-- 5 -->
    <div class="row phase-5 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">开通微信认证需要资料 (上傳）INFORMATION REQUIRED TO ACTIVATE WECHAT OFFICIAL ACCOUNT (UPLOAD)</div>
                <div class="card-body">
                  <p>开通微信认证需要准备以下材料，准备好后开始申请将极大提高申请效率</p>
                  <p class=" m-0">TO APPLY WECHAT OFFICIAL ACCOUNT, YOU NEED TO PREPARE THE FOLLOWING DOCUMENTS TO IMPROVE APPLICATION EFFICIENCY.</p>
									<ul>
										<!-- <li>ACRA Biz File Image</li> -->
										<li>Passport Image</li>
										<li>NRIC Image</li>
										<li>Phone Bill Image</li>
										<li>ACRA Biz Image</li>
										<li>Application Form (with company stamp)</li>
									</ul>
                </div>
              </div>
            </div>

						<input type="hidden" name="access_token" id="access_token" value="{{ $token }}">
            <!-- <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">下载企业申请表 <b>DOWNLOAD APPLICATION FORM</b></div>
									<input type="hidden" name="access_token" id="access_token" value="{{ $token }}">
                  <p class="m-0 font-base-sm">Please Download Here: <a target="_blank" href="{{ url('/WechatApplicationForm/'.$token ) }}">{{ url('/WechatApplicationForm/'.$token ) }}</a></p>
                </div>
              </div>
            </div> -->


            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册证明文件(最新的) THE UPDATED ACRA BIZ FILE (.JPG)：</div>
									<p class="font-base-sm">Note : Not more than 3 months from date of application.</p>
                  <p class="font-base-sm">Example : </p>

                  <div class="text-center">
                    <img src="{{ asset('img/bonaimg/Wechat_banner_Biz File A4.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('acra_biz_file');"><i class="fa fa-upload"></i> Upload ACRA Biz File</button>

                  <textarea class="hide" name="acra_biz_file" id="acra_biz_file"></textarea>
                  <div class="">
                    <div id="acra_biz_file_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员护照 PASSPORT OF THE OPERATING ACCOUNT OPERATOR (.JPG) ：</div>
                  <!-- <p class="font-base-sm">Example : <a target="_blank" href="http://bit.ly/CityFlowExampleNRIC">http://bit.ly/CityFlowExampleNRIC</a></p> -->
									<p class="font-base-sm">Note : Passport Image must be clear with no reflection</p>
									<p class="font-base-sm">Example :</p>

                  <div class="text-center">
                    <img src="{{ asset('img/bonaimg/Wechat_banner_passport A4.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('oa_passport_image');"><i class="fa fa-upload"></i> Add File</button>

                  <textarea class="hide" name="oa_passport_image" id="oa_passport_image"></textarea>
                  <div class="">
                    <div id="oa_passport_image_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员登记 NRIC OF THE OA OPERATOR (.JPG) NRIC</div>
                  <!-- <p class="font-base-sm">Example : <a href="http://bit.ly/CityFlowExampleNRIC">http://bit.ly/CityFlowExampleNRIC</a> </p> -->
									<p class="font-base-sm">Example : </p>

                  <div class="text-center">
                    <img src="{{ asset('img/bonaimg/Wechat_banner IC_ A4.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('oa_nric_image');"><i class="fa fa-upload"></i> Add File</button>

                  <textarea class="hide" name="oa_nric_image" id="oa_nric_image"></textarea>
                  <div class="">
                    <div id="oa_nric_image_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员手机电话账单电子档 (最近三个月)( * 公司电话帐单请加盖公司章) LAST 3</div>
                  <div class="h6">MONTHS TELEPHONE RECEIPT OF OA OPERATOR. ( * PLEASE AFFIX THE COMPANY STAMP TO THE COMPANY TELEPHONE BILL) .(.JPG) :</div>
                  <!-- <p class="font-base-sm">Example : <a target="_blank" href="https://bit.ly/CityFlowExamplePhoneBill">https://bit.ly/CityFlowExamplePhoneBill</a> </p> -->
									<p>NOTE: Please ensure company stamp is visible.</p>
									<p class="font-base-sm">Example : </p>

                  <div class="text-center">
                    <img src="{{ asset('img/bonaimg/Wechat_banner_Singtel bill A4.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('oa_months_telephone_receipt_image');"><i class="fa fa-upload"></i> Add File</button>

                  <textarea class="hide" name="oa_months_telephone_receipt_image" id="oa_months_telephone_receipt_image"></textarea>
                  <div class="">
                    <div id="oa_months_telephone_receipt_image_preview"></div>
                  </div>
                </div>
              </div>
            </div>

						<!-- Wechat_banner_stamp A4 -->
            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">微信认证申请公函电子档 (请加盖公司章 ) APPLICATION FORM (PLEASE AFFIX THE COMPANY STAMP) (.JPG) :</div>
                  <!-- <p class="font-base-sm">Example : <a target="_blank" href="https://bit.ly/CityFlowExampleWeChatApplication">https://bit.ly/CityFlowExampleWeChatApplication</a> </p> -->
									<p>NOTE: Please ensure company stamp is visible.</p>
									<p class="font-base-sm">Example : </p>

                  <div class="text-center">
                    <img src="{{ asset('img/bonaimg/Wechat_banner_stamp A4.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('application_form_image');"><i class="fa fa-upload"></i> Add File</button>

                  <textarea class="hide" name="application_form_image" id="application_form_image"></textarea>
                  <div class="">
                    <div id="application_form_image_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">《商标注册书》和《商标授权书》（可选）(.JPG)</div>
                  <p class="font-base-sm">如果公众号包含商标名称，需要上传此材料 "Trademark Registration Form" and "Trademark Authorization Letter" (optional) (.JPG) If the official account contains a brand name, this material needs to be uploaded</p>

                  <button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('trade_mark_authorization_image');"><i class="fa fa-upload"></i> Add File</button>

                  <textarea class="hide" name="trade_mark_authorization_image" id="trade_mark_authorization_image"></textarea>
                  <div class="">
                    <div id="trade_mark_authorization_image_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6 bold">CITYFLOW PTE LTD</div>
                  <p class="font-base-sm m-0">5008 Ang Mo Kio Ave 5, #04-09 Techplace II, Singapore 569874 </p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">PDPA Policy - ACKNOWLEDGEMENT AND CONSENT  <span class="text-danger">*</span></div>
									<!-- <p class="font-base-sm">I confirm that by providing my personal data and contact details above, I have agreed for Cityflow Ptd Ltd and its authorized agents/employees to use the messages (in sound, text, visual and other form) including all provided documents, to apply for WeChat Official Account in connection with Cityflow Pte Ltd's business, in accordance with the Personal Data Protection Act (PDPA), until such time I inform Cityflow Pte Ltd in writing, otherwise.</p> -->
                  <p class="font-base-sm">I, consent to the collection, use and disclosure of the company and its directors' corporate and personal data to be utilised by Cityflow Pte Ltd for the purpose of the application for an Official WeChat Account only.</p>

									<div class=" mb-4">
										<div class="mb-2 mt-4">
												<p class="m-1">FULL NAME IN NRIC :</p>
											<input type="text" placeholder="Your Answer" name="nric_full_name" id="nric_full_name" class="full">
											<div class="invalid-feedback" id="err_nric_full_name"></div>
										</div>


										<div class="mb-2 mt-4">
												<p class="m-1">NRIC NUMBER (LAST 3 DIGITS & LAST ALPHABETS) :</p>
											<input type="text" placeholder="Your Answer"  name="nric_number" id="nric_number" class="full">
											<div class="invalid-feedback" id="err_nric_number"></div>
										</div>

										<div class="mb-2 mt-4">
											<p class="m-1">DESIGNATION :</p>
											<input type="text" placeholder="Your Answer" name="designation" id="designation" class="full">
											<div class="invalid-feedback" id="err_designation"></div>
										</div>


										<div class="mb-2 mt-4">
												<p class="m-1">COMPANY NAME :</p>
											<input type="text" placeholder="Your Answer" name="nric_company_name" id="nric_company_name" class="full">
											<div class="invalid-feedback" id="err_nric_company_name"></div>
										</div>


										<div class="mb-2 mt-4">
												<p class="m-1">UNIQUE ENTITY NUMBER (UEN) :</p>
											<input type="text" placeholder="Your Answer"  name="uen" id="uen" class="full">
											<div class="invalid-feedback" id="err_uen"></div>
										</div>
									</div>

									<br>
                  <div class="">
                      <div class="form-check font-base-lg">
                        <input class="form-check-input" type="radio" name="pdpa_policy_true" id="pdpa_policy_true" value="yes">
                        <label class="form-check-label" for="pdpa_policy_true">
                          I acknowledge & consent
                        </label>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-sm-12 pr-0 mr-0">
                          <div class="form-check font-base-lg mr-0 pr-0">
                            <input class="form-check-input" type="radio" name="pdpa_policy_true" id="pdpa_policy_false" value="no">
                            <label class="form-check-label" for="pdpa_policy_false">
                          	I do not acknowledge & consent
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-10 ml-4">
                          <input type="text" placeholder="Your Answer" name="pdpa_policy_others" id="pdpa_policy_others" class="full">
													<div class="invalid-feedback" id="err_pdpa_policy_others"></div>
                          	<div class="invalid-feedback" id="err_pdpa_policy_others"></div>
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>


            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="$('#submit').val(0); next('phase-4', 'phase-1', 'phase-2', 'phase-3', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-dark font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="submit"><b>Submit</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div>


		<!-- <div class="row phase-6 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

						<div class="col-sm-12">
							<div class="card card-form-base text-left">
								<div class="card-body">
									<p>FULL NAME IN NRIC :</p>
									<div class="">
										<input type="text" placeholder="Your Answer" name="nric_full_name" id="nric_full_name" class="full">
									</div>
								</div>
							</div>
						</div>


						<div class="col-sm-12">
							<div class="card card-form-base text-left">
								<div class="card-body">
									<p>NRIC NUMBER (LAST 3 DIGITS & LAST ALPHABETS) :</p>
									<div class="">
										<input type="text" placeholder="Your Answer"  name="nric_number" id="nric_number" class="full">
									</div>
								</div>
							</div>
						</div>


						<div class="col-sm-12">
							<div class="card card-form-base text-left">
								<div class="card-body">
									<p>DESIGNATION :</p>
									<div class="">
										<input type="text" placeholder="Your Answer" name="designation" id="designation" class="full">
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="card card-form-base text-left">
								<div class="card-body">
									<p>COMPANY NAME :</p>
									<div class="">
										<input type="text" placeholder="Your Answer" name="nric_company_name" id="nric_company_name" class="full">
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12">
							<div class="card card-form-base text-left">
								<div class="card-body">
									<div class="h6">企业注册号码 :</div>
									<p class="">UNIQUE ENTITY NUMBER (UEN) :</p>
									<div class="">
										<input type="text" placeholder="Your Answer"  name="uen" id="uen" class="full">
									</div>
								</div>
							</div>
						</div>



            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="$('#submit').val(0); next('phase-5', 'phase-1', 'phase-2', 'phase-4', 'phase-3', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-dark font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="submit"><b>Submit</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div> -->


    <!-- 6 -->
    <!-- <div class="row phase-6 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">Credit Card Payment</div>
                <div class="card-body">
            			<a href="#" class="btn btn-default"><h3>Payment Link</h3></a>
									<br>
                  <br>
                  <div class="text-center">
                    <img src="{{ asset('img/payment.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <br>
									<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('payment_wechat_account_annual_fee');"><i class="fa fa-upload"></i> Screen shot the payment slip and upload here</button>

                  <textarea class="hide" name="payment_wechat_account_annual_fee" id="payment_wechat_account_annual_fee"></textarea>
									<div class="invalid-feedback" id="err_payment_wechat_account_annual_fee"></div>


                  <div class="">
                    <div id="payment_wechat_account_annual_fee_preview"></div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="$('#submit').val(0); next('phase-5', 'phase-1', 'phase-2', 'phase-4', 'phase-3', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-dark font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="submit"><b>Submit</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div> -->


	</center>
	</form>
</body>
	@include('Layout.footer', ['type' => 'home'])
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous"></script>

</html>

@include("Include.modal_preview")

<script type="text/javascript">
	function next(show, hide1, hide2, hide3, hide4, hide5){
		let str = show.replace("phase-", "");
		$("#page").val(str);
		$("."+show).removeClass('hide');
		$("."+show).addClass('animated fadeIn');
    $("."+hide1).addClass('hide');
    $("."+hide2).addClass('hide');
    $("."+hide3).addClass('hide');
    $("."+hide4).addClass('hide');
		$("."+hide5).addClass('hide');
	}

	$("#form_add_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
				// swal("Please Wait", "Processing...", "info");
				loading();
			},
			success:function(response){
				$("#submit").val(response.submit);
				$("#page").val(response.page);
				swal.close();
				console.log(response);
				if (response.status == true && response.page == 6) {
					// console.log(response);
					window.location = main_path+'/application/submit/'+response.id;
					showValidator(response.error,'form_add_form');
				}else{
					showValidator(response.error,'form_add_form');
				}

			},
			error:function(error){
				console.log(error);
			}
		});
	});

</script>
<!--
else if (response.status == true && response.page == 6) {
	// console.log(response);
	window.location = main_path+'/application/submit/'+response.id;
	showValidator(response.error,'form_add_form');
} -->
