<!DOCTYPE html>
<html>
<style media="screen">
/* Container needed to position the overlay. Adjust the width as needed */
.img-container {
position: relative;
width: 100%;
}

/* Make the image to responsive */
.image {
display: block;
width: 100%;
height: auto;
background-repeat: no-repeat;
background-color: #555
}

/* The overlay effect - lays on top of the container and over the image */
.overlay {
position: absolute;
bottom: 0;
background: rgb(0, 0, 0);
background: rgba(0, 0, 0, 0.9); /* Black see-through */
color: #f1f1f1;
width: 100%;
transition: .5s ease;
opacity:0.8;
color: white;
font-size: 15px;
padding: 20px;
text-align: center;
}
.img-bg{
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat !important;
}

/* When you mouse over the container, fade in the overlay title */
.img-container:hover .overlay {
opacity: 1;
}
</style>
 @include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
<body class="font-base" style="background-color: #ebebeb;">
  <div class="container-fluid mt-2 mb-3">
    <div class="card">
      <div class="card-header">
        <div class="h4">
          {{ $app->company_name }}
        </div>
      </div>
      <div class="card-body text-center">
        <div class="row">
          @if(!empty($app->acra_biz_file))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->acra_biz_file) }}" style="background-image: url({{ asset('upload/'.$app->acra_biz_file) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">ACRA biz</div>
            </div>
          </div>
          @endif

          @if(!empty($app->oa_passport_image))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->oa_passport_image) }}" style="background-image: url({{ asset('upload/'.$app->oa_passport_image) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">OA Passport</div>
            </div>
          </div>
          @endif


          @if(!empty($app->oa_nric_image))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->oa_nric_image) }}" style="background-image: url({{ asset('upload/'.$app->oa_nric_image) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">OA NRIC</div>
            </div>
          </div>
          @endif

          @if(!empty($app->oa_months_telephone_receipt_image))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->oa_months_telephone_receipt_image) }}" style="background-image: url({{ asset('upload/'.$app->oa_months_telephone_receipt_image) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">OA Tel. Receipt</div>
            </div>
          </div>
          @endif

          @if(!empty($app->application_form_image))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->application_form_image) }}" style="background-image: url({{ asset('upload/'.$app->application_form_image) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">Application Form</div>
            </div>
          </div>
          @endif

          @if(!empty($app->trade_mark_authorization_image))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->trade_mark_authorization_image) }}" style="background-image: url({{ asset('upload/'.$app->trade_mark_authorization_image) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">Trademark Authorization</div>
            </div>
          </div>
          @endif

          @if(!empty($app->payment_wechat_account_annual_fee))
          <div class="col-sm-2 mb-4">
            <div class="img-container">
              <img src="{{ asset('img/img.png') }}" data-file="{{ asset('upload/'.$app->payment_wechat_account_annual_fee) }}" style="background-image: url({{ asset('upload/'.$app->payment_wechat_account_annual_fee) }})" alt="Avatar" class="image img-thumbnail pointer img-bg">
              <div class="overlay">Payment</div>
            </div>
          </div>
          @endif


        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" role="dialog" id="preview_images">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <div class="modal-title">
          Preview
          </div>
          <button class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
         <div class="text-center zoom" id="zoomer" style="cursor: zoom-in;">
           <img src="" alt="" id="img_preview" class="img-thumbnail img-fluid zoomer">
         </div>
        </div>
        <div class="modal-footer">

        </div>
      </div>
    </div>
  </div>
</body>
 @include('Layout.footer', ['type' => 'home'])
 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.21/jquery.zoom.min.js" integrity="sha512-m5kAjE5cCBN5pwlVFi4ABsZgnLuKPEx0fOnzaH5v64Zi3wKnhesNUYq4yKmHQyTa3gmkR6YeSKW1S+siMvgWtQ==" crossorigin="anonymous"></script>

</html>

<script type="text/javascript">
$(".img-container img").on('click', function(){
  let src = $(this).attr("data-file");
  $("#img_preview").attr("src", src);
  $('#zoomer').zoom();
  $("#preview_images").modal("show");
  // console.log(src);
});
</script>
