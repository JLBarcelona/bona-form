<!DOCTYPE html>
<style type="text/css">
	.bg{
		background-position: center;
		background-size: cover;
	}
	.card-form{
		text-align: center;
		margin-top: 10px;
	    max-height: 22.440528634361232vw;
	    max-width: 90vw;
	    height: 159.5770925110132px;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-form-base{
		margin-top: 10px;
	    max-width: 90vw;
	    height: auto;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-1{
		background-image: url(https://lh5.googleusercontent.com/zdX8VsNyWBRMdQps8rr9Abf0qbPwAILvbm8tJ3URtqJQHczIpIZ0Qi-1dLuSK-XPhjz3_JBTl4mxSuEtTM0o9YG2XdaZnNyfyIeFnqzKjKXqun2KUtQSitH1gAm4=w1135);
	}
	.buttons-group{
		padding-left: 17px !important;
	}
	input {
	  outline: 0;
	  border-width: 0 0 2px;
	  border-color: #c1c1c1;
	  transition: border-color 0.3s;
	}
	input:focus {
	  border-color: #a1a1a1;
	}
	.show{
		display: '';
	}
	.hide{
		display: none;
	}
  .full{
    width: 100%;
  }
  .font-base-sm{
    font-size: 13px;
  }
</style>

<style media="screen">
.label-info{
	background: #563d7c !important;
	color: white !important;
	padding: 3px;
	border-radius: 5px;
	margin: 5px;
	}
.bootstrap-tagsinput{
	margin-top: 10px;
	width: 100% !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
}
.bootstrap-tagsinput>input{
	margin-top: 10px !important;
	width: 400px !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;

}
</style>
<!-- https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135 -->
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha512-wu4jn1tktzX0SHl5qNLDtx1uRPSj+pm9dDgqsrYUS16AqwzfdEmh1JR8IQL7h+phL/EAHpbBkISl5HXiZqxBlQ==" crossorigin="anonymous" />

<body class="font-base" style="background-color: #ebebeb;">
		<input type="hidden" value="5" name="submit" id="submit">
		<input type="hidden" value="{{ $form->application_form_id }}" name="application_form_id" id="application_form_id">
	<center>
	<div class="container-fluid mt-2 mb-3">
		<div class="row phase-1 hide">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">微信公众帐号注册及认证申请表 (服务号)</div>
								<p>WECHAT OFFICIAL ACCOUNT - REGISTRATION AND VERIFICATION APPLICATION FORM (SERVICE ACCOUNT)</p>
								<input type="text" placeholder="Email Address" style="width: 100%;" name="email_address" id="email_address">
								<div class="invalid-feedback" id="err_email_address"></div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm"><b>Next</b></button>
							</div>
						</div>
					</div>

				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>


    <!-- 2 -->
    <div class="row phase-2">
  			<div class="col-sm-3 col-lg-3 col-md-3"></div>
  			<div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">企业基本资料 COMPANY INFORMATION</div>
                <div class="card-body">
                  <p>需与当地政府颁发的企业注册证或商业登记证书等企业证件上的企业名称保持完全一致，信息审核成功后，企业名称不可修改。</p>
                  <p>THE COMPANY NAME MUST BE COMPLETELY CONSISTENT WITH THE COMPANY REGISTRATION CERTIFICATE ISSUED BY THE LOCAL GOVERNMENT. AFTER THE INFORMATION IS SUCCESSFULLY APPROVED, THE COMPANY NAME CANNOT BE MODIFIED.</p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业名称 :</div>
                  <p>COMPANY NAME (ALL CAPITAL) :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_name" id="company_name" value="{{ $form->company_name }}" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册号码 :</div>
                  <p>COMPANY REGISTRATION NUMBER :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" onkeypress="return num_only(event);" value="{{ $form->company_registration_number }}" name="company_registration_number" id="company_registration_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册地址 :</div>
                  <p>COMPANY REGISTRATION ADDRESS :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_registration_address" id="company_registration_address" value="{{ $form->company_registration_address }}" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业网站：</div>
                  <p>COMPANY WEBSITE :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="company_website" id="company_website" class="full" value="{{ $form->company_website }}">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <!-- <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-1', 'phase-2', 'phase-3', 'phase-4', 'phase-5', 'phase-6'); $('#submit').val(0);" type="button"><b>Back</b></button> -->
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-3', 'phase-1', 'phase-2', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
  	</div>

    <!-- 3 -->
    <div class="row phase-3 hide">
  			<div class="col-sm-3 col-lg-3 col-md-3"></div>
  			<div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">管理员信息登记</div>
                <div class="card-body">
                  <p>INFORMATION OF THE OFFICIAL ACCOUNT OPERATOR ("OA OPERATOR")</p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">

                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img src="https://lh5.googleusercontent.com/1XSiy2KvsvKzK_D4vDusAdVCsVIsgtdShFJ2fu4CXjWC531Mq3YgLji_nUy4WF7yql_vDHN1-Auv75DgvuESq12-TgtXiLoyrBy1jOFQTuO8jUoeDK85j1yH4kSA=w1236" alt="phone" class="img-fluid">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员姓名("帳號管理员") :</div>
                  <p>FULL NAME OF THE OFFICIAL ACCOUNT OPERATOR ("OA OPERATOR") :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="oa_operator" id="oa_operator" class="full" value="{{ $form->oa_operator }}">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员的护照号码及护照签发国 :</div>
                  <p>OA OPERATOR'S PASSPORT NUMBER AND ISSUING COUNTRY :</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" value="{{ $form->oa_operator_passport_number }}" onkeypress="return num_only(event)" name="oa_operator_passport_number" id="oa_operator_passport_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员手机号 :</div>
                  <p>MOBILE PHONE NUMBER OF OA OPERATOR :</p>
                  <div class="">
                    <input type="text" onkeypress="return num_only(event)" value="{{ $form->oa_operator_mobile_number }}" placeholder="Your Answer" name="oa_operator_mobile_number" id="oa_operator_mobile_number" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号管理员座机: (座机用于联繫不到您时使用)</div>
                  <p>LANDLINE OF OA OPERATOR (THE LANDLINE IS USED WHEN WE CAN'T REACH YOU)</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="oa_landline" id="oa_landline" class="full" value="{{ $form->oa_landline }}">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-2', 'phase-1', 'phase-3', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-4', 'phase-1', 'phase-2', 'phase-3', 'phase-5', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
  	</div>

    <!-- 4 -->
    <div class="row phase-4 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">微信帐号资料 WECHAT OFFICIAL ACCOUNT INFORMATION</div>
                <div class="card-body">
                  <p class="m-0">微信认证帐号命名规则:</p>
                  <p class="m-0">保护注册商标原则：帐号名不得侵犯注册商标专用权，否则审核将不被通过或面临法律诉讼；</p>
                  <p class="m-0">认证命名唯一原则：帐号名不得与已认证帐号重复，否则审核将不被通过。</p>
                  <br>
                  <p class="m-0">命名方式</p>
                  <p class="m-0">基于自选词汇命名  基于商标命名</p>
                  <br>
                  <p class="font-base-sm m-0">NAME RULES OF THE WECHAT OFFICIAL ACCOUNT :</p>
                  <p class="font-base-sm m-0">THE PRINCIPLE OF PROTECTING REGISTERED TRADEMARKS : ACCOUNT NAMES MUST NOT INFRINGE </p>
                  <p class="font-base-sm m-0">ON THE EXCLUSIVE RIGHTS OF REGISTERED TRADEMARKS, OTHERWISE THE AUDIT WILL NOT BE  </p>
                  <p class="font-base-sm m-0">APPROVED OR LEGAL PROCEEDINGS WILL BE FACED; </p>
                  <p class="font-base-sm m-0">THE ONLY PRINCIPLE OF CERTIFICATION NAME: THE ACCOUNT NAME MUST NOT BE THE SAME AS THE  </p>
                  <p class="font-base-sm m-0">CERTIFIED ACCOUNT, OTHERWISE THE AUDIT WILL NOT BE PASSED. </p>
                  <br>
                  <p class="font-base-sm m-0">NAME METHOD: </p>
                  <p class="font-base-sm m-0">NAME BASED ON SELF-SELECTED VOCABULARY OR NAME BASED ON TRADEMARK</p>

                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">请按照优先顺序，(30/30, 4~30个字符)（1个汉字算2个字符）提供至少三个微信公众帐号名称</div>
                  <p>PLEASE PROVIDE AT LEAST 3 WECHAT OFFICIAL ACCOUNT NAMES ACCORDING TO PRIORITY</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" value="{{ $form->wechat_officials_account_names }}" name="wechat_officials_account_names" id="wechat_officials_account_names" class="full">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">帐号功能介绍 (4~120个字) : (指企业服务项目介绍)</div>
                  <p>ACCOUNT INTRODUCTION (4-120 WORDS) (COMPANY SERVICE INTRODUCTION)</p>
                  <div class="">
                    <input type="text" placeholder="Your Answer" name="account_introduction" id="account_introduction" class="full" value="{{ $form->account_introduction }}">
                  </div>
                </div>
              </div>
            </div>



            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-3', 'phase-1', 'phase-2', 'phase-4', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-5', 'phase-1', 'phase-2', 'phase-3', 'phase-4', 'phase-6');" type="button"><b>Next</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div>


    <!-- 5 -->
    <div class="row phase-5 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">开通微信认证需要资料 (上傳）INFORMATION REQUIRED TO ACTIVATE WECHAT OFFICIAL ACCOUNT (UPLOAD)</div>
                <div class="card-body">
                  <p>开通微信认证需要准备以下材料，准备好后开始申请将极大提高申请效率</p>
                  <p class=" m-0">TO APPLY WECHAT OFFICIAL ACCOUNT, YOU NEED TO PREPARE THE FOLLOWING DOCUMENTS TO IMPROVE APPLICATION EFFICIENCY.</p>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">下载企业申请表 <b>DOWNLOAD APPLICATION FORM</b></div>
                  <p class="m-0 font-base-sm">Please Download Here: <a target="_blank" href="{{ url('/WechatApplicationForm/'.$form->access_token ) }}">{{ url('/WechatApplicationForm/'.$form->access_token ) }}</a></p>
                </div>
              </div>
            </div>


            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">企业注册证明文件(最新的) THE UPDATED ACRA BIZ FILE (.JPG)：</div>
                  <p class="font-base-sm">Example : <a target="_blank" href="https://bit.ly/CityFlowExampleArca">https://bit.ly/CityFlowExampleArca</a></p>

                  <div class="text-center">
                    <img src="https://lh6.googleusercontent.com/dKh8uAac6qYwfI1FSbGdXbWKccBC8bWJgURzTjB63HZt-Zy7XzykHon2pU7podGIR29X3Vq1ZOF-RNHbLSUn5iGne6tjlR2v0UDGSwt1za9FEKFIbcjAi9hAsuyL=w740" alt="phone" class="img-fluid">
                  </div>

                  <textarea class="hide" name="acra_biz_file" id="acra_biz_file"></textarea>
                  <div class="">
										<div id="acra_biz_file_preview">
											 @if(!empty($form->acra_biz_file))
												 <img src="{{ asset('upload/'.$form->acra_biz_file) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
											 @endif
									 </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员护照 PASSPORT OF THE OA OPERATOR (.JPG) ：</div>
                  <p class="font-base-sm">Example : <a target="_blank" href="http://bit.ly/CityFlowExampleNRIC">http://bit.ly/CityFlowExampleNRIC</a></p>

                  <div class="text-center">
                    <img src="https://lh5.googleusercontent.com/YYdiz9VH9s2jQERAxINSPLK0kggckCLpCSOJ3aElQpxfWRiQRX-jNOzDN0LXFY8ssHjekJI5WLqqRiHHcBjUdU3qdrX9yI4L9m9o9ER-oSR7eAbeK17LshcIH5TG=w740" alt="phone" class="img-fluid">
                  </div>

                  <textarea class="hide" name="oa_passport_image" id="oa_passport_image"></textarea>
                  <div class="">
										<div id="oa_passport_image_preview">
											@if(!empty($form->oa_passport_image))
												<img src="{{ asset('upload/'.$form->oa_passport_image) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
											@endif
										</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员登记 NRIC OF THE OA OPERATOR (.JPG) NRIC</div>
                  <p class="font-base-sm">Example : <!-- <a href="http://bit.ly/CityFlowExampleNRIC">http://bit.ly/CityFlowExampleNRIC</a> --> </p>

                  <div class="text-center">
                    <img src="https://lh6.googleusercontent.com/FHXAyr-gEPxTrLAUzwBxNB74O4LnNKcTftbZX-2ski3VrhZ7NWef2txHwicvSgKxE66mn18zRtuj7bCo1gkj-OwqDcZRd4hU1b-1-Mispxr5eYXeCrnZs9TqKppc=w740" alt="phone" class="img-fluid">
                  </div>

                  <textarea class="hide" name="oa_nric_image" id="oa_nric_image"></textarea>
                  <div class="">
										<div id="oa_nric_image_preview">
											@if(!empty($form->oa_nric_image))
												<img src="{{ asset('upload/'.$form->oa_nric_image) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
											@endif
										</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">管理员手机电话账单电子档 (最近三个月)( * 公司电话帐单请加盖公司章) LAST 3</div>
                  <div class="h6">MONTHS TELEPHONE RECEIPT OF OA OPERATOR. ( * PLEASE AFFIX THE COMPANY STAMP TO THE COMPANY TELEPHONE BILL) .(.JPG) :</div>
                  <p class="font-base-sm">Example : <a target="_blank" href="https://bit.ly/CityFlowExamplePhoneBill">https://bit.ly/CityFlowExamplePhoneBill</a> </p>

                  <div class="text-center">
                    <img src="https://lh5.googleusercontent.com/_eo4M462ZG4c7Zg2OAhfE2LBH2TRN-__jmaR95STlHQy8Lzazy9AByyU4onzSK0MDPIA6sHFKa9qRkOlb_WDCJgF9kD-5GbCbkYaxac1hq0tEC8KqsWlGiA6KfM7=w740" alt="phone" class="img-fluid">
                  </div>

                  <textarea class="hide" name="oa_months_telephone_receipt_image" id="oa_months_telephone_receipt_image"></textarea>
                  <div class="">
										<div id="oa_months_telephone_receipt_image_preview">
												@if(!empty($form->oa_months_telephone_receipt_image))
													<img src="{{ asset('upload/'.$form->oa_months_telephone_receipt_image) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
												@endif
											</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">微信认证申请公函电子档 (请加盖公司章 ) APPLICATION FORM (PLEASE AFFIX THE COMPANY STAMP) (.JPG) :</div>
                  <p class="font-base-sm">Example : <a target="_blank" href="https://bit.ly/CityFlowExampleWeChatApplication">https://bit.ly/CityFlowExampleWeChatApplication</a> </p>

                  <div class="text-center">
                    <img src="https://lh4.googleusercontent.com/wdWq2bie1mawTKhIKZ9M16qRHarxcQsFqBDwm0Cb88mZh7PF1PgPx3XhcbBFGge8SK3M29UEB_Rt1Ibtv7GlXGWNx7N5zAIwZU8BMUXQVz07ZO29t-sePWbhjk5b=w740" alt="phone" class="img-fluid">
                  </div>

                  <textarea class="hide" name="application_form_image" id="application_form_image"></textarea>
                  <div class="">
										<div id="application_form_image_preview">
										 @if(!empty($form->application_form_image))
											 <img src="{{ asset('upload/'.$form->application_form_image) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
										 @endif
									 </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">《商标注册书》和《商标授权书》（可选）(.JPG)</div>
                  <p class="font-base-sm">如果公众号包含商标名称，需要上传此材料 "Trademark Registration Form" and "Trademark Authorization Letter" (optional) (.JPG) If the official account contains a brand name, this material needs to be uploaded</p>


                  <textarea class="hide" name="trade_mark_authorization_image" id="trade_mark_authorization_image"></textarea>
                  <div class="">
										<div id="trade_mark_authorization_image_preview">
											@if(!empty($form->trade_mark_authorization_image))
												<img src="{{ asset('upload/'.$form->trade_mark_authorization_image) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
											@endif
										</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6 bold">CITYFLOW PTE LTD</div>
                  <p class="font-base-sm m-0">5008 Ang Mo Kio Ave 5, #04-09 Techplace II, Singapore 569874 </p>
                </div>
              </div>
            </div>

						<div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-body">
                  <div class="h6">PDPA Policy 个人隐私保护政策 <span class="text-danger">*</span></div>
                  <p class="font-base-sm">I confirm that by providing my personal data and contact details above, I have agreed for Cityflow Ptd Ltd and its authorized agents/employees to use the messages (in sound, text, visual and other form) including all provided documents, to apply for WeChat Official Account in connection with Cityflow Pte Ltd's business, in accordance with the Personal Data Protection Act (PDPA), until such time I inform Cityflow Pte Ltd in writing, otherwise.</p>
                  <br>
                  <div class="">
										@php $yes = ($form->pdpa_policy_true == 1)? 'checked' : '' @endphp
										@php $no = ($form->pdpa_policy_true == 0)? 'checked' : '' @endphp

                      <div class="form-check font-base-lg">
                        <input class="form-check-input" type="radio" name="pdpa_policy_true" id="pdpa_policy_true" value="yes" {{ $yes }}>
                        <label class="form-check-label" for="pdpa_policy_true">
                          Yes
                        </label>
                      </div>
                      <br>
                      <div class="row">
                        <div class="col-sm-1 pr-0 mr-0">
                          <div class="form-check font-base-lg mr-0 pr-0">
                            <input class="form-check-input" type="radio" name="pdpa_policy_true" id="pdpa_policy_false" value="no" {{ $no }}>
                            <label class="form-check-label" for="pdpa_policy_false">
                            Other:
                            </label>
                          </div>
                        </div>
                        <div class="col-sm-10 ml-4">
                          <input type="text" placeholder="Your Answer" name="pdpa_policy_others" id="pdpa_policy_others" value="{{ $form->pdpa_policy_others }}" class="full">
													<div class="invalid-feedback" id="err_pdpa_policy_others"></div>
                          	<div class="invalid-feedback" id="err_pdpa_policy_others"></div>
                        </div>
                      </div>

                  </div>
                </div>
              </div>
            </div>


            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-4', 'phase-1', 'phase-2', 'phase-3', 'phase-5', 'phase-6');" type="button"><b>Back</b></button>
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-6', 'phase-1', 'phase-2', 'phase-3', 'phase-4', 'phase-5');" type="button"><b>Next</b></button>

                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div>


    <!-- 6 -->
    <div class="row phase-6 hide">
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
        <div class="col-sm-4 col-lg-6 col-md-6">
          <center>
            <!-- card 1 -->
            <div class="col-sm-12">
              <div class="card card-form card-1 bg">
                <div class="card-body">
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left">
                <div class="card-header bg-dark text-white">Credit Card Payment</div>
                <div class="card-body">
                  <!-- <div class="h6">微信公众号年审费 SGD145 (USD99) PAYMENT FOR WECHAT OFFICIAL ACCOUNT ANNUAL FEE of SGD145 (USD99)</div> -->
									<a href="#" class="btn btn-default">Payment Link</a>
									<br>
                  <br>
                  <div class="text-center">
                    <img src="{{ asset('img/payment.jpg') }}" alt="phone" class="img-fluid">
                  </div>
                  <br>

                  <textarea class="hide" name="payment_wechat_account_annual_fee" id="payment_wechat_account_annual_fee"></textarea>
                  <div class="">
                    <div id="payment_wechat_account_annual_fee_preview">
											@if(!empty($form->payment_wechat_account_annual_fee))
												<img src="{{ asset('upload/'.$form->payment_wechat_account_annual_fee) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
											@endif
										</div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="card card-form-base text-left border-0" style="background: transparent;">
                <div class="card-body p-0">
                  <button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" onclick="next('phase-5', 'phase-1', 'phase-2', 'phase-4', 'phase-3', 'phase-6'); $('#submit').val(5);" type="button"><b>Back</b></button>
                </div>
              </div>
            </div>

          </center>
        </div>
        <div class="col-sm-3 col-lg-3 col-md-3"></div>
    </div>


	</center>

</body>
	@include('Layout.footer', ['type' => 'home'])
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous"></script>

</html>

 <div class="modal fade" role="dialog" id="modal_upload">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Upload
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
          	 <input type="hidden" name="input" id="input">
          	 <div class="text-center">
          	 	<div id="preview_img"></div>
          	 </div>
      			  <div class="custom-file">
      			    <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/jpeg,image/jpg" id="file" name="file" required>
      			    <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
      			  </div>
          </div>
          <div class="modal-footer text-right">
            <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
            <button class="btn btn-dark" onclick="upload();">Upload</button>
          </div>
        </div>
      </div>
    </div>

@include("Include.modal_preview")

<script type="text/javascript">
	$("#wechat_officials_account_names").tagsinput();


	function show_upload(param){
		// Preview
		$("#input").val(param);
		$("#modal_upload").modal('show');
	}

	function previewFile() {
	  const att = $("#input").val();
	  const preview = $("#"+att+"_preview");
	  const base64 = $("#"+att);
	  const file = document.querySelector('input[type=file]').files[0];
	  const reader = new FileReader();

	  reader.addEventListener("load", function () {
	  	base64.val(reader.result);
	  	$("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mb-2" width="150">');
	  }, false);

	  if (file) {
	    reader.readAsDataURL(file);
	  }
	}


	function upload(){
    const att = $("#input").val();
    const preview = $("#"+att+"_preview");
    const base64 = $("#"+att).val();
		 preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">');
		 $("#modal_upload").modal('hide');
		 $("#input").val('');
	  	 $("#preview_img").html('');
	}

	function close(){
		 $("#modal_upload").modal('hide');
		 $("#input").val('');
	  	 $("#preview_img").html('');
	}


	function next(show, hide1, hide2, hide3, hide4, hide5){
		$("."+show).removeClass('hide');
		$("."+show).addClass('animated fadeIn');
    $("."+hide1).addClass('hide');
    $("."+hide2).addClass('hide');
    $("."+hide3).addClass('hide');
    $("."+hide4).addClass('hide');
		$("."+hide5).addClass('hide');
	}

	$('input[type=text]').prop("readonly", true);
	$('input[type=radio]').prop("disabled", true);


</script>
