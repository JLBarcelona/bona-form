<!DOCTYPE html>
<html>
<style media="screen">
/* Container needed to position the overlay. Adjust the width as needed */
.img-container {
position: relative;
width: 100%;
}

/* Make the image to responsive */
.image {
display: block;
width: 100%;
height: auto;
background-repeat: no-repeat;
background-color: #555
}

/* The overlay effect - lays on top of the container and over the image */
.overlay {
position: absolute;
bottom: 0;
background: rgb(0, 0, 0);
background: rgba(0, 0, 0, 0.9); /* Black see-through */
color: #f1f1f1;
width: 100%;
transition: .5s ease;
opacity:0.8;
color: white;
font-size: 15px;
padding: 20px;
text-align: center;
}
.img-bg{
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat !important;
}

/* When you mouse over the container, fade in the overlay title */
.img-container:hover .overlay {
opacity: 1;
}
</style>
 @include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
<body class="font-base">
  <div class="container-fluid mt-5 mb-3">
    <div class="col-sm-12 mt-5 text-center">
      <br><br><br>
      <img src="{{ asset('img/logo.jpg') }}" class="img-fluid" alt="" width="250">
      <h1>WeChat Application Form</h1>
      <h3>You can only access this link once you submitted the form.</h1>
        <br><br>
        <p>If you already submitted, try to reload the page.</p>
        <button type="button" name="button" class="btn btn-success" onclick="location.reload()"><i class="fa fa-sync"></i> Reload</button>
    </div>
  </div>
</body>
 @include('Layout.footer', ['type' => 'home'])
</html>
