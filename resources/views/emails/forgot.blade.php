@component('mail::message', ['data' => $data])
# Hi {{ $data['name'] }},

You recently requested to reset your password for BonaForm.
Click the button below to reset it.

@component('mail::button', ['url' => $data['url_link']])
 Reset your Password
@endcomponent

If you did not request a password reset, Please ignore this email or reply to let us know.
This password reset is only valid for the next 10 minutes.

Thanks,<br>
BonaForm
@endcomponent