@component('mail::message', ['data' => $data])
# Hi {{ $data['name_admin'] }},

{{ $data['name'] }}, submited a request form from BonaForm.

@component('mail::button', ['url' => $data['url_link']])
 View Request from {{ $data['name'] }}
@endcomponent

Thanks,<br>
BonaForm
@endcomponent
