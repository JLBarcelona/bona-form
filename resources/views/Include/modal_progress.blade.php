<div class="modal fade" role="dialog" id="modal_progress">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Please wait..
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="progress-text h6 text-center">
        </div>
        <div class="progress">
    		  <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%"></div>
    		</div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  function show_progress(status, progress){
    if (status == true) {
      $("#modal_progress").modal({"backdrop": "static"});
      $(".progress").attr('aria-valuenow', progress);
    }else{
      $("#modal_progress").modal("hide");
      $(".progress").attr('aria-valuenow', 0);
    }
  }
</script>
