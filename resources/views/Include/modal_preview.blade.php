<div class="modal fade" role="dialog" id="modal_upload">
   <div class="modal-dialog">
     <div class="modal-content">
       <div class="modal-header">
         <div class="modal-title">
         Upload
         </div>
         <button class="close" data-dismiss="modal">&times;</button>
       </div>
       <div class="modal-body">
          <input type="hidden" name="input" id="input">
          <div class="text-center">
           <div id="preview_img"></div>
          </div>
           <div class="custom-file">
             <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/jpeg,image/jpg" id="file" name="file" required>
             <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
           </div>
       </div>
       <div class="modal-footer text-right">
         <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
         <button class="btn btn-dark" onclick="upload();">Upload</button>
       </div>
     </div>
   </div>
 </div>

<div class="modal fade" role="dialog" id="preview_images">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        Preview
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
       <div class="text-center zoom" id="zoomer">
         <img src="" alt="" id="img_preview" class="img-thumbnail img-fluid zoomer">
       </div>
      </div>
      <div class="modal-footer">

      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-zoom/1.7.21/jquery.zoom.min.js" integrity="sha512-m5kAjE5cCBN5pwlVFi4ABsZgnLuKPEx0fOnzaH5v64Zi3wKnhesNUYq4yKmHQyTa3gmkR6YeSKW1S+siMvgWtQ==" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ asset('js/form.js') }}"></script>
