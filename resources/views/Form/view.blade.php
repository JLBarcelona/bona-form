<!DOCTYPE html>
<style type="text/css">
	.bg{
		background-position: center;
		background-size: cover;
	}
	.card-form{
		text-align: center;
		margin-top: 10px;
	    max-height: 22.440528634361232vw;
	    max-width: 90vw;
	    height: 159.5770925110132px;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-form-base{
		margin-top: 10px;
	    max-width: 90vw;
	    height: auto;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-1{
		background-image: url(https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135);
	}
	.buttons-group{
		padding-left: 17px !important;
	}
	input {
	  outline: 0 !important;
	  border-width: 0 0 2px !important;
	  border-color: #c1c1c1 !important;
	  transition: border-color 0.3s !important;
	}
	input:focus {
	  border-color: #a1a1a1 !important;
	}
	.show{
		display: '';
	}
	.hide{
		display: none;
	}
</style>

<style media="screen">
.label-info{
	background: #563d7c !important;
	color: white !important;
	padding: 3px;
	border-radius: 5px;
	margin: 5px;
	}
.bootstrap-tagsinput{
	margin-top: 10px;
	width: 100% !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
}
.bootstrap-tagsinput>input{
	margin-top: 10px !important;
	width: 400px !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;

}
</style>
<!-- https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135 -->
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha512-wu4jn1tktzX0SHl5qNLDtx1uRPSj+pm9dDgqsrYUS16AqwzfdEmh1JR8IQL7h+phL/EAHpbBkISl5HXiZqxBlQ==" crossorigin="anonymous" />

<body class="font-base" style="background-color: #ebebeb;">
	<form class="needs-validation" id="form_add_form" action="{{ url('form/add_form') }}" novalidate>
		<input type="hidden" value="0" name="submit" id="submit">
		<input type="hidden" value="{{ $form->form_id }}" name="form_id" id="form_id">
	<center>
	<div class="container-fluid mt-2 mb-3">
		<div class="row phase-1">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<!-- card 1 -->
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">微信号设置 (帐号详情)</div>
								<p>SET UP WECHAT OFFICIAL ACCOUNT (KEYWORD)</p>
								<!-- <p>The name and photo associated with your Google account will be recorded when you upload files and submit this form. Not johnluisb14@gmail.com? Switch account</p> -->
								<input type="text" placeholder="Key in Account Name" style="width: 100%;" value="{{ $form->account_name }}" name="account_name" id="account_name" disabled>
								<div class="invalid-feedback" id="err_account_name"></div>
							</div>
						</div>
					</div>``

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">修改头像</div>
								<p>一个月可申请修改5次新头像, 不允许涉及政治敏感与色情; 修改头像需经过审核; 图片格式只支持：BMP、JPEG、JPG、GIF、PNG，大小不超过2M CHANGE OF AVATAR You can apply to change new avatars 5 times a month, political sensitivity and pornography are not allowed; change of avatars needs to be approved. (Image formats: BMP, JPEG, JPG, GIF, PNG, and the size does not exceed 2M)</p>
								<div class="text-center">
									<img src="https://lh4.googleusercontent.com/J93qGllHRU4CfKudYb4aMxvzbWaAok3HYQaDaRCCqGfKSfvwZsNUXNSlS41UlJ1YNl_LliJAxgm7NB6MzFHnsSUFazgRqCjegvTK96OCkSolhmnZb_G5ZzsDul4x=w740" alt="phone" class="img-fluid">
								</div>
								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('avatar');"><i class="fa fa-upload"></i> Add Company Logo</button>

								<textarea class="hide" name="avatar" id="avatar"></textarea>
								<div class="">
									<div id="avatar_preview">
                    @if(!empty($form->avatar))
                      <img src="{{ asset('upload/'.$form->avatar) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
                    @endif
                  </div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">客服电话</div>
								<p>CUSTOMER SERVICE PHONE #</p>
								<div class="text-center">
									<img src="https://lh5.googleusercontent.com/51p4PKcCoqrNvuuagADyLYlMz4_j8j-W-ve7LN8PbrsE-HDYyRTNZhCh4ynyvxD7q-T910WvcR1fuqBv81JJkFJbM22FJmcf_zxzFfAjoDcHd8hhXdAXiJheG3WQ=w740" alt="phone" class="img-fluid">
								</div>
								<div class="col-sm-12 p-0">
									<input type="text" style="width:100%;" value="{{ $form->customer_service_phone }}" disabled placeholder="Number" onkeypress="return num_only(event);" name="customer_service_phone" id="customer_service_phone">
								</div>
							</div>
						</div>


					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="button" onclick="$('#submit').val(0); next('phase-2', 'phase-1');"><b>Next</b></button>
							</div>
						</div>
					</div>

				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>

		<div class="row phase-2 hide">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<!-- card 1 -->
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<!-- <div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">公众号设置 (帐号详情)</div>
								<p>SET UP WECHAT OFFICIAL ACCOUNT (ACCOUNT DETAILED INFORMATION)</p>
								<p>The name and photo associated with your Google account will be recorded when you upload files and submit this form. Not johnluisb14@gmail.com? Switch account</p>
							</div>
						</div>
					</div> -->

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header bg-dark text-white">素材管理</div>
							<div class="card-body">
								<p>公众号菜单设置 <br>
								MATERIALS MANAGEMENT<br>
								Cover Menu Set Up</p>

							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">产品商标</div>
								<p>Banner Logo</p>
								<div class="text-center">
									<img src="https://lh4.googleusercontent.com/GceRB3puieqTOOFGUz7yqtQfzThyeI7Fq1O0rTGKemUWWITCmygvVZxh-1bS1pMA1aj-cQyEZ8du83n33LVSDq-05IK8pVZzjrtfonPy0g5ZhduDaa6P7p5L4mf-=w740" alt="phone" class="img-fluid">
								</div>

								<textarea class="hide" name="trademark_logo" id="trademark_logo"></textarea>
								<div class="">
									<div id="trademark_logo_preview">
                    @if(!empty($form->trademark_logo))
                      <img src="{{ asset('upload/'.$form->trademark_logo) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
                    @endif
                  </div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置 (左边）</div>
								<p>COVER MENU (LEFT) (PLEASE KEY IN THE NAME OF THE MENU)</p>
								<div class="text-center">
									<img src="https://lh6.googleusercontent.com/5diqBvkB9fo16zhCbdQwH5WquMXE67lsaWPq3U9xIkTPpMp3Gtosb-mF0s2ODmkvPJ1R-2y8MHWIHaO4dWGJbyEvuwPCZkxKb2PTA_h2VLSuEJnwhJGhVKaulZ7t=w740" alt="phone" class="img-fluid">
								</div>
								<div class="col-sm-12 p-0">
									<input type="text" class="mt-1" placeholder="Your Answer" value="{{ $form->cover_menu_left ?? '' }}" data-role="tagsinput" id="cover_menu_left" name="cover_menu_left" style="width:100%;">
								</div>
								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="" placeholder="Picture name" id="left_menu_picture_name" value="{{ $form->menu_picture_left }}" name="left_menu_picture_name" style="width:100%;">
									<div class="invalid-feedback" id="err_left_menu_picture_name"></div>
								</div>
								<textarea class="hide" name="menu_picture_left" id="menu_picture_left"></textarea>
								<div class="">
									<div id="menu_picture_left_preview">
                    @if(!empty($form->menu_picture_left))
                      <img src="{{ asset('upload/'.$form->menu_picture_left) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
                    @endif
                  </div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置(中间)</div>
								<p>COVER MENU (CENTER)</p>
								<div class="text-center">
									<img src="https://lh5.googleusercontent.com/AmcCDi1kHX1b4JGqsIza4j2i_tVdrA3E6oP5Qg8-bkGTbuI_n4dVb0EzkvpS924XtoD6dYGKvwNZMluk0iXk8Xjb1zQhYI2ySTtbKiz9dqBfk00VRSQyk0WL_VAq=w740" alt="phone" class="img-fluid">
								</div>
								<div class="col-sm-12 p-0">
									<input type="text" class="mt-1" placeholder="Your Answer" id="cover_menu_center" value="{{ $form->cover_menu_center ?? '' }}" name="cover_menu_center" style="width:100%;">
								</div>

								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="mt-1" placeholder="Picture name" id="center_menu_picture_name" value="{{ $form->menu_picture_center }}" name="center_menu_picture_name" style="width:100%;">
										<div class="invalid-feedback" id="err_center_menu_picture_name"></div>
								</div>

								<textarea class="hide" name="menu_picture_center" id="menu_picture_center"></textarea>
								<div class="">
									<div id="menu_picture_center_preview">
                    @if(!empty($form->menu_picture_center))
                      <img src="{{ asset('upload/'.$form->menu_picture_center) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
                    @endif
                  </div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置 (右边)</div>
								<p>COVER MENU (RIGHT)</p>
								<div class="text-center">
									<img src="https://lh5.googleusercontent.com/S-pqG7PfKbSqwn8aEm3ByroYu31BlZO9iXT92meyf4ba2pyrPHKPzsWxNMyj-0LPZgc6UfOOffAne6uvKme9haNdB93WMLnZ-C8kx58U8O-Xuc2rQfWp0P_bozcs=w740" alt="phone" class="img-fluid">
								</div>
								<div class="col-sm-12 p-0">
									<input type="text" class="mt-1" placeholder="Your Answer" name="cover_menu_right" id="cover_menu_right" value="{{ $form->cover_menu_right }}" style="width:100%;">
								</div>

								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="mt-1" placeholder="Picture name" id="right_menu_picture_name" name="right_menu_picture_name" value="{{ $form->menu_picture_right }}" style="width:100%;">
										<div class="invalid-feedback" id="err_right_menu_picture_name"></div>
								</div>

								<textarea class="hide" name="menu_picture_right" id="menu_picture_right"></textarea>
								<div class="">
									<div id="menu_picture_right_preview">
                    @if(!empty($form->menu_picture_right))
                      <img src="{{ asset('upload/'.$form->menu_picture_right) }}" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">
                    @endif
                  </div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="button" onclick="$('#submit').val(0); next('phase-1', 'phase-2');"><b>Back</b></button>
							</div>
						</div>
					</div>


				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>
	</div>
	</center>
	</form>
</body>
	@include('Layout.footer', ['type' => 'home'])
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous"></script>

</html>


 <div class="modal fade" role="dialog" id="modal_upload">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <div class="modal-title">
            Upload
            </div>
            <button class="close" data-dismiss="modal">&times;</button>
          </div>
          <div class="modal-body">
          	 <input type="hidden" name="input" id="input">
          	 <div class="text-center">
          	 	<div id="preview_img"></div>
          	 </div>
			  <div class="custom-file">
			    <input type="file" class="custom-file-input" onchange="previewFile();" accept="image/*" id="file" name="file" required>
			    <label class="custom-file-label" for="validatedCustomFile">Choose file</label>
			  </div>
          </div>
          <div class="modal-footer text-right">
            <button class="btn btn-default" onclick="close();" data-dismiss="modal">Close</button>
            <button class="btn btn-dark" onclick="upload();">Upload</button>
          </div>
        </div>
      </div>
    </div>

	@include("Include.modal_preview")

<script type="text/javascript">

	$("#cover_menu_left").tagsinput();
	$("#cover_menu_center").tagsinput();
	$("#cover_menu_right").tagsinput();
	$("#account_name").tagsinput();
	// account_name

	function show_upload(param){
		// Preview
		$("#input").val(param);
		$("#modal_upload").modal('show');
	}

	function previewFile() {
	  const att = $("#input").val();
	  const preview = $("#"+att+"_preview");
	  const base64 = $("#"+att);
	  const file = document.querySelector('input[type=file]').files[0];
	  const reader = new FileReader();

	  reader.addEventListener("load", function () {
	  	base64.val(reader.result);
	  	$("#preview_img").html('<img src="'+reader.result+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mb-2" width="150">');
	  }, false);

	  if (file) {
	    reader.readAsDataURL(file);
	  }
	}


	function upload(){
  		 const att = $("#input").val();
	 	 const preview = $("#"+att+"_preview");
  		 const base64 = $("#"+att).val();
		 preview.html('<img src="'+base64+'" alt="" class="img-fluid img-thumbnail animated jackInTheBox mt-2" width="150">');
		 $("#modal_upload").modal('hide');
		 $("#input").val('');
	  	 $("#preview_img").html('');
	}

	function close(){
		 $("#modal_upload").modal('hide');
		 $("#input").val('');
	  	 $("#preview_img").html('');
	}


	function next(show, hide){
		$("."+show).removeClass('hide');
		$("."+show).addClass('animated fadeIn');
		$("."+hide).addClass('hide');
		$(".bootstrap-tagsinput :input").attr("disabled", true);
		// $(".bootstrap-tagsinput>input").attr('disabled', true);
	}

$('input[type=text]').prop("readonly", true);

</script>
