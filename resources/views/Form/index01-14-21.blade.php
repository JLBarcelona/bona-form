<!DOCTYPE html>
<style type="text/css">
	.bg{
		background-position: center;
		background-size: cover;
	}
	.card-form{
			text-align: center;
			margin-top: 10px;
	    max-height: 22.440528634361232vw;
	    max-width: 90vw;
	    height: 159.5770925110132px;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-form-base{
		margin-top: 10px;
	    max-width: 90vw;
	    height: auto;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-1{
		background-image: url(https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135);
	}
	.buttons-group{
		padding-left: 17px !important;
	}
	input {
	  outline: 0 !important;
	  border-width: 0 0 2px !important;
	  border-color: #c1c1c1 !important;
	  transition: border-color 0.3s !important;
	}
	input:focus {
	  border-color: #a1a1a1 !important;
	}
	.show{
		display: '';
	}
	.hide{
		display: none;
	}
</style>

<style media="screen">
.label-info{
	background: #563d7c !important;
	color: white !important;
	padding: 3px;
	border-radius: 5px;
	margin: 5px;
	}
.bootstrap-tagsinput{
	margin-top: 10px;
	width: 100% !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
}
.bootstrap-tagsinput>input{
	margin-top: 10px !important;
	width: 400px !important;
	outline: 0 !important;
	border-width: 0 0 2px !important;
	border-color: #c1c1c1 !important;
	transition: border-color 0.3s !important;
	box-shadow: inset 0 0px 0px rgba(0, 0, 0, 0) !important;
	border-radius: 0px !important;

}
.img-bg{
  background-position: center;
  background-size: cover;
  background-repeat: no-repeat !important;
}
</style>


<!-- https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135 -->
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])

	<link rel="stylesheet" href="{{ asset('css/phone.css') }}">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" integrity="sha512-xmGTNt20S0t62wHLmQec2DauG9T+owP9e6VU8GigI0anN7OXLip9i7IwEhelasml2osdxX71XcYm6BQunTQeQg==" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" integrity="sha512-wu4jn1tktzX0SHl5qNLDtx1uRPSj+pm9dDgqsrYUS16AqwzfdEmh1JR8IQL7h+phL/EAHpbBkISl5HXiZqxBlQ==" crossorigin="anonymous" />

<body class="font-base" style="background-color: #ebebeb;">
	<form class="needs-validation" id="form_add_form" action="{{ url('form/add_form') }}" novalidate>
		<input type="hidden" value="0" name="submit" id="submit">
		<input type="hidden" value="1" name="page" id="page">
	<center>
	<div class="container-fluid mt-2 mb-3">
		<div class="row phase-1">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<!-- card 1 -->
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">微信号设置 (帐号详情)</div>
								<p>SET UP WECHAT OFFICIAL ACCOUNT (KEYWORD)</p>
								<!-- <p>The name and photo associated with your Google account will be recorded when you upload files and submit this form. Not johnluisb14@gmail.com? Switch account</p> -->
								<div class="mb-2">
									<input type="text" placeholder="Key in Account Name(Option 1)" style="width: 100%;" name="account_name" id="account_name">
									<div class="invalid-feedback" id="err_account_name"></div>
								</div>

								<div class="mb-2">
									<input type="text" placeholder="Key in Account Name(Option 2)" style="width: 100%;" name="account_name2" id="account_name2">
									<div class="invalid-feedback" id="err_account_name2"></div>
								</div>

								<div class="mb-2">
									<input type="text" placeholder="Key in Account Name(Option 3)" style="width: 100%;" name="account_name3" id="account_name3">
									<div class="invalid-feedback" id="err_account_name3"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">修改头像</div>
								<p>一个月可申请修改5次新头像, 不允许涉及政治敏感与色情; 修改头像需经过审核; 图片格式只支持：BMP、JPEG、JPG、GIF、PNG，大小不超过2M CHANGE OF AVATAR You can apply to change new avatars 5 times a month, political sensitivity and pornography are not allowed; change of avatars needs to be approved. (Image formats: BMP, JPEG, JPG, GIF, PNG, and the size does not exceed 2M)</p>
								<div class="text-center">
									<img src="https://lh4.googleusercontent.com/J93qGllHRU4CfKudYb4aMxvzbWaAok3HYQaDaRCCqGfKSfvwZsNUXNSlS41UlJ1YNl_LliJAxgm7NB6MzFHnsSUFazgRqCjegvTK96OCkSolhmnZb_G5ZzsDul4x=w740" alt="phone" class="img-fluid phone">
								</div>
								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('avatar');"><i class="fa fa-upload"></i> Add Company Logo</button>

								<textarea class="hide" name="avatar" id="avatar"></textarea>
								<div class="">
									<div id="avatar_preview"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">客服电话</div>
								<p>CUSTOMER SERVICE PHONE #</p>
								<div class="text-center">
									<img src="https://lh5.googleusercontent.com/51p4PKcCoqrNvuuagADyLYlMz4_j8j-W-ve7LN8PbrsE-HDYyRTNZhCh4ynyvxD7q-T910WvcR1fuqBv81JJkFJbM22FJmcf_zxzFfAjoDcHd8hhXdAXiJheG3WQ=w740" alt="phone" class="img-fluid phone">
								</div>
								<div class="col-sm-12 p-0">
									<input type="text" style="width:100%;" placeholder="Number" onkeypress="return num_only(event);" name="customer_service_phone" id="customer_service_phone">
								</div>
							</div>
						</div>


					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm"><b>Next</b></button>
							</div>
						</div>
					</div>

				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>

		<div class="row phase-2 hide">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<!-- card 1 -->
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<!-- <div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">公众号设置 (帐号详情)</div>
								<p>SET UP WECHAT OFFICIAL ACCOUNT (ACCOUNT DETAILED INFORMATION)</p>
								<p>The name and photo associated with your Google account will be recorded when you upload files and submit this form. Not johnluisb14@gmail.com? Switch account</p>
							</div>
						</div>
					</div> -->

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header bg-dark text-white">素材管理</div>
							<div class="card-body">
								<p>公众号菜单设置 <br>
								MATERIALS MANAGEMENT<br>
								Cover Menu Set Up</p>

							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">产品商标</div>
								<p>Banner Logo</p>
								<div class="text-center">
									<img src="https://lh4.googleusercontent.com/GceRB3puieqTOOFGUz7yqtQfzThyeI7Fq1O0rTGKemUWWITCmygvVZxh-1bS1pMA1aj-cQyEZ8du83n33LVSDq-05IK8pVZzjrtfonPy0g5ZhduDaa6P7p5L4mf-=w740" alt="phone" class="img-fluid phone">
								</div>
								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('trademark_logo');"><i class="fa fa-upload"></i> Add Banner Logo</button>

								<textarea class="hide" name="trademark_logo" id="trademark_logo"></textarea>
								<div class="">
									<div id="trademark_logo_preview"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置 (左边）</div>
								<p>COVER MENU (LEFT) (PLEASE KEY IN THE NAME OF THE MENU)</p>

								<div class="smartphone">
								  <div class="text-center">
										<nav class="navbar navbar-light bg-light" style="top:450px;">
											<ul class="nav nav-tabs justify-content-center">
										  <li class="nav-item dropup show">
										    <a class="nav-link" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false">三VITAYES</a>
										    <div class="dropdown-menu show">
													<input type="text" class="dropdown-item form-control" name="cover_menu_left1" id="cover_menu_left1" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_left2" id="cover_menu_left2" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_left3" id="cover_menu_left3" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_left4" id="cover_menu_left4" value="">
										    </div>
										  </li>
											<li class="nav-item">
											 <a class="nav-link active" href="javascript:;">最新活动</a>
										 </li>
										  <li class="nav-item">
												<a class="nav-link active" href="javascript:;">客户服务</a>
										  </li>
										</ul>
										</nav>
								  </div>
								</div>
								<div class="text-center">
									<!-- <img src="{{ asset('img/phone_logo/COVER MENU (LEFT).jpg') }}" alt="phone" class="img-fluid phone img-bg" width="300"> -->
								</div>
								<!-- <div class="col-sm-12 p-0">
									<input type="text" class="mt-1" placeholder="Your Answer" data-role="tagsinput" id="cover_menu_left" name="cover_menu_left" style="width:100%;">
								</div> -->
								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="" placeholder="Picture name" id="left_menu_picture_name" name="left_menu_picture_name" style="width:100%;">
									<div class="invalid-feedback" id="err_left_menu_picture_name"></div>
								</div>

								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('menu_picture_left');"><i class="fa fa-upload"></i> Add Content & High Resolution Picture</button>

								<textarea class="hide" name="menu_picture_left" id="menu_picture_left"></textarea>
								<div class="">
									<div id="menu_picture_left_preview"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置(中间)</div>
								<p>COVER MENU (CENTER)</p>
								<div class="text-center">
									<!-- <img src="{{ asset('img/phone_logo/COVER MENU (CENTER).jpg') }}" alt="phone" class="img-fluid phone img-bg" width="300"> -->
								</div>

								<div class="smartphone">
									<div class="text-center">
										<nav class="navbar navbar-light bg-light" style="top:450px;">
											<ul class="nav nav-tabs justify-content-center">
											<li class="nav-item">
												<a class="nav-link active" href="javascript:;">三VITAYES</a>
											</li>
											<li class="nav-item dropup show">
												<a class="nav-link" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false">最新活动</a>
												<div class="dropdown-menu show">
													<input type="text" class="dropdown-item form-control" name="cover_menu_center1" id="cover_menu_center1" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_center2" id="cover_menu_center2" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_center3" id="cover_menu_center3" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_center4" id="cover_menu_center4" value="">
												</div>
											</li>
											<li class="nav-item">
												<a class="nav-link active" href="javascript:;">客户服务</a>
											</li>
										</ul>
										</nav>
									</div>
								</div>

								<div class="col-sm-12 p-0">
									<!-- <input type="text" class="mt-1" placeholder="Your Answer" id="cover_menu_center" name="cover_menu_center" style="width:100%;"> -->
								</div>

								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="mt-1" placeholder="Picture name" id="center_menu_picture_name" name="center_menu_picture_name" style="width:100%;">
										<div class="invalid-feedback" id="err_center_menu_picture_name"></div>
								</div>

								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('menu_picture_center');"><i class="fa fa-upload"></i> Add Content & High Resolution Picture</button>

								<textarea class="hide" name="menu_picture_center" id="menu_picture_center"></textarea>
								<div class="">
									<div id="menu_picture_center_preview"></div>
								</div>

							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-body">
								<div class="h6">菜单设置 (右边)</div>
								<p>COVER MENU (RIGHT)</p>
								<div class="text-center">
									<!-- <img src="{{ asset('img/phone_logo/COVER MENU (RIGHT).jpg') }}" alt="phone" class="img-fluid phone img-bg" width="300"> -->
								</div>

								<div class="smartphone">
									<div class="text-center">
										<nav class="navbar navbar-light bg-light" style="top:450px;">
											<ul class="nav nav-tabs justify-content-center">
											<li class="nav-item">
												<a class="nav-link active" href="javascript:;">三VITAYES</a>
											</li>
											<li class="nav-item">
												<a class="nav-link active" href="javascript:;">最新活动</a>
											</li>
											<li class="nav-item dropup show">
												<a class="nav-link" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false">客户服务</a>
												<div class="dropdown-menu show" style="margin-left:-90px;">
													<input type="text" class="dropdown-item form-control" name="cover_menu_right1" id="cover_menu_right1" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_right2" id="cover_menu_right2" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_right3" id="cover_menu_right3" value="">
													<input type="text" class="dropdown-item form-control" name="cover_menu_right4" id="cover_menu_right4" value="">
												</div>
											</li>

										</ul>
										</nav>
									</div>
								</div>

								<!-- <div class="col-sm-12 p-0">
									<input type="text" class="mt-1" placeholder="Your Answer" name="cover_menu_right" id="cover_menu_right" style="width:100%;">
								</div> -->

								<div class="col-sm-12 p-0">
									<br>
									<p class="">Add Picture and Content (Indicate Picture Name) Prefer in PowerPoint.</p>
									<input type="text" class="mt-1" placeholder="Picture name" id="right_menu_picture_name" name="right_menu_picture_name" style="width:100%;">
										<div class="invalid-feedback" id="err_right_menu_picture_name"></div>
								</div>

								<button class="btn btn-light text-primary mt-1 border" type="button" onclick="show_upload('menu_picture_right');"><i class="fa fa-upload"></i> Add Content & High Resolution Picture</button>
								<textarea class="hide" name="menu_picture_right" id="menu_picture_right"></textarea>
								<div class="">
									<div id="menu_picture_right_preview"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left border-0" style="background: transparent;">
							<div class="card-body p-0">
								<button class="btn btn-default font-base-lg pl-3 pr-3 mt-2 shadow-sm" type="button" onclick="$('#submit').val(0); $('#page').val(1); next('phase-1', 'phase-2');"><b>Back</b></button>
								<button class="btn btn-dark font-base-lg pl-3 pr-3 mt-2 shadow-sm border-white"><b>Submit</b></button>
							</div>
						</div>
					</div>


				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>
	</div>
	</center>
	</form>
</body>
	@include('Layout.footer', ['type' => 'home'])
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js" integrity="sha512-9UR1ynHntZdqHnwXKTaOm1s6V9fExqejKvg5XMawEMToW4sSw+3jtLrYfZPijvnwnnE8Uol1O9BcAskoxgec+g==" crossorigin="anonymous"></script>

</html>

@include("Include.modal_preview")

<script type="text/javascript">

	$("#cover_menu_left").tagsinput();
	$("#cover_menu_center").tagsinput();
	$("#cover_menu_right").tagsinput();
	// $("#account_name").tagsinput();
	// account_name

	function next(show, hide){
		$("."+show).removeClass('hide');
		$("."+show).addClass('animated fadeIn');
		$("."+hide).addClass('hide');
	}

	$("#form_add_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
			loading();
			// swal("Please Wait", "Processing...", "info");
			},
			success:function(response){
					swal.close();
					console.log(response);
					$("#submit").val(response.submit);

				if(response.status == true  && response.submit == 2){
					console.log(response);
					// swal("Success", response.message, "success");
					window.location = main_path+'/submit/'+response.id;
					showValidator(response.error,'form_add_form');
				}else if (response.submit == 0) {
					showValidator(response.error,'form_add_form');
				}else if (response.submit == 1 && $("#page").val() == 1) {
   					showValidator(response.error,'form_add_form');
						next('phase-2', 'phase-1');
						$("#page").val(response.page);
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'form_add_form');
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});
	//
	// else if (response.status == false && response.submit == 2) {
	// 	showValidator(response.error,'form_add_form');
	// 	$("#submit").val(2);
	// }else if (response.status == true && $("#submit").val() == 0) {
	// 	next('phase-2', 'phase-1');
	// 	$("#submit").val(1);
	// }

</script>
