<!DOCTYPE html>
<style type="text/css">
	.bg{
		background-position: center;
		background-size: cover;
	}
	.card-form{
		text-align: center;
		margin-top: 10px;
	    max-height: 22.440528634361232vw;
	    max-width: 90vw;
	    height: 159.5770925110132px;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-form-base{
		margin-top: 10px;
	    max-width: 90vw;
	    height: auto;
	    width: 640px;
	    border-radius: 10px !important;
	}
	.card-1{
		background-image: url(https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135);
	}
	.buttons-group{
		padding-left: 17px !important;
	}
	input {
	  outline: 0;
	  border-width: 0 0 2px;
	  border-color: #c1c1c1;
	  transition: border-color 0.3s;
	}
	input:focus {
	  border-color: #a1a1a1;
	}
	.show{
		display: '';
	}
	.hide{
		display: none;
	}
</style>
<!-- https://lh5.googleusercontent.com/7TVwwLFTks7-MGe2fyFyvM8bfubXu9cQ0cfifsPqInqj7QQxwiKEtuHLKtgbi2PWY5qA-OMZQiGGUWKviTGPSVYnneuFtTtvIuHGg2YOCV7YATagjmP7VeYH5GC0=w1135 -->
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Form', 'icon' => asset('img/logophone.png') ])
<body class="font-base" style="background-color: #ebebeb;">
	<form class="needs-validation" id="form_add_form" action="{{ url('form/add_form') }}" novalidate>
		<input type="hidden" value="0" name="submit" id="submit">
	<center>
	<div class="container-fluid mt-2 mb-3">
		<div class="row phase-1">
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
			<div class="col-sm-4 col-lg-6 col-md-6">
				<center>
					<!-- card 1 -->
					<div class="col-sm-12">
						<div class="card card-form card-1 bg">
							<div class="card-body">
							</div>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="card card-form-base text-left">
							<div class="card-header p-1 bg-dark"></div>
							<div class="card-body">
								<div class="h3">公众号设置 (帐号详情)</div>
								<p>Your response has been recorded.</p>
								<p><a href="{{ url('form') }}">Submit another response</a></p>
							</div>
						</div>
					</div>

				</center>
			</div>
			<div class="col-sm-3 col-lg-3 col-md-3"></div>
		</div>

	</div>
	</center>
	</form>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>
	