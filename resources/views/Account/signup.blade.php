<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Register', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'home'])
	<div class="container-fluid mobile-margin">
		<div class="row">
			<div class="col-sm-4"></div>
			<div class="col-sm-4 p-0">

				<div class="card">
					<div class="card-header font-base-xl">Register</div>
					<div class="card-body">
						<form class="needs-validation" id="user_register_form" action="{{ url('user/add') }}" novalidate>
						<div class="form-row">
							<div class="form-group col-sm-12">
								<label>Name </label>
								<input type="text" id="name" name="name" placeholder="Name" class="form-control " required>
								<div class="invalid-feedback" id="err_name"></div>
							</div>
							<div class="form-group col-sm-12">
								<label>Username </label>
								<input type="text" id="username" name="username" placeholder="Username" class="form-control " required>
								<div class="invalid-feedback" id="err_username"></div>
							</div>
							<div class="form-group col-sm-12">
								<label>Email </label>
								<input type="email" id="email" name="email" placeholder="Email" class="form-control " required>
								<div class="invalid-feedback" id="err_email"></div>
							</div>
							<div class="form-group col-sm-12">
								<label>Password </label>
								<input type="password" id="password" name="password" placeholder="Password" class="form-control " required>
								<div class="invalid-feedback" id="err_password"></div>
							</div>
							<div class="form-group col-sm-12">
								<label>Confirm Password </label>
								<input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" class="form-control " required>
								<div class="invalid-feedback" id="err_confirm_password"></div>
							</div>

							<div class="form-group col-sm-12">
								<div class="row">
									<div class="col-sm-10">
											<p class="alert alert-danger alert-captcha hide" style="width: 90%;"></p>
											<div class="g-recaptcha float-left" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
											<div class="invalid-feedback" id="err_g-recaptcha-response"></div
												>
									</div>
									<div class="col-sm-2">
										<span class="form-group btn-login-mobile">
											<button type="submit" class="btn btn-primary btn-sm" id="btn_reg">Register</button>
											<a href="https://bona.com.sg/" target="_blank" class="btn btn-dark btn-sm" style="margin: 8px 0;">Support</a>
										</span>
									</div>
								</div>
							</div>
						</div>
					</form>

					</div>
					<div class="card-footer"></div>
				</div>
			</div>
			<div class="col-sm-4"></div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>

<script type="text/javascript">
$("#user_register_form").on('submit', function(e){
	var url = $(this).attr('action');
	var mydata = $(this).serialize();
	e.stopPropagation();
	e.preventDefault(e);

	$.ajax({
		type:"POST",
		url:url,
		data:mydata,
		cache:false,
		beforeSend:function(){
				//<!-- your before success function -->
				swal("Loading...","Please wait \n we will sent a account verification to your email!","info");
				$("#btn_reg").attr("disabled", true);
		},
		success:function(response){

			if(response.status == true){
				$("#btn_reg").attr("disabled", false);
				// console.log(response)
				// swal("Success",response.message,"success");
				swal({
					title: "Success",
					text: response.message,
					type: "success",
					showCancelButton: false,
					confirmButtonColor: "#007bff",
					confirmButtonText: "Okay",
				},
				function(isConfirm){
					if (isConfirm) {
						window.location = main_path + '/';
					}
				});
				showValidator(response.error,'user_register_form');
			}else{
				//<!-- your error message or action here! -->
				 check_captcha(response.error['g-recaptcha-response']);
				showValidator(response.error,'user_register_form');
			}
		},
		error:function(error){
			console.log(error)
		}
	});
});
</script>


<script>
$(".g-recaptcha-response").on('input', function(){
	$('.alert-captcha').hide('fast');
	return true;
});


function check_captcha(check_res){
	if (typeof check_res == 'undefined') {
		$('.alert-captcha').hide('fast');
		return true;
	}else{
		if(check_res.length > 0){
			$('.alert-captcha').show('fast');
			$('.alert-captcha').text('Please input captcha field');
			setTimeout(function(){
				$('.alert-captcha').hide('slow');
			},3000);
			return false;
		}

	}
}
</script>