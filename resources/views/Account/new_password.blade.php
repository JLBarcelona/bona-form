<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'New Password', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'home'])
	<div class="container-fluid mobile-margin">
		<div class="row mt-5 mb-3">
			<div class="col-sm-4 text-center">
			</div>
			<div class="col-sm-4 p-0">
				<div class="text-center">
					<h4 class="text-capitalize">reset password</h4>
					<br>
				</div>
				<div class="card mbt-5 rounded-lg">
					<div class="card-body">
						<form class="needs-validation" id="form_new_password" action="{{ url('/create_new_password/'.$token.'/'.$id.'') }}" novalidate>
							<div class="form-row">
								<div class="form-group col-sm-12">
									<label>New Password </label>
									<input type="password" id="new_password" name="new_password" placeholder="New Password" class="form-control " required>
									<div class="invalid-feedback" id="err_new_password"></div>
								</div>
								<div class="form-group col-sm-12">
									<label>Confirm Password </label>
									<input type="password" id="confirm_password" name="confirm_password" placeholder="Confirm Password" class="form-control " required>
									<div class="invalid-feedback" id="err_confirm_password"></div>
								</div>
								<div class="form-group col-sm-12 ">
									<p class="alert alert-danger alert-captcha hide"></p>
									<span class="form-group" style="float:right; display: inline-grid;">
										<button type="submit" class="btn btn-primary btn-sm">Log in</button>
										<a href="https://bona.com.sg/" target="_blank" class="btn btn-dark btn-sm" style="margin: 8px 0;">Support</a>
									</span>
									<div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
									<div class="invalid-feedback" id="err_g-recaptcha-response"></div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="text-center mt-4">
					<div class="row">
						<div class="col-sm-12">
							<a href="https://bona.com.sg/" target="_blank" class="btn-sm bold font-base-lg text-dark">Bona ERP by Bona Technologies</a>
						</div>
						<div class="col-sm-6">
							<img src="{{ asset('img/sgd.jpg') }}" alt="" class="img-fluid">
						</div>
						<div class="col-sm-6">
							<img src="{{ asset('img/infocom.jpg') }}" alt="" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-4 text-center">
				<!-- <img src="{{ asset('img/questions.png') }}" class="img-fluid animated slideInLeft" style="width:62%;"> -->
			</div>
		</div>
	</div>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>

  <script>
	$("#form_new_password").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				// console.log(response);
				if(response.status == true){
     		 swal({
                  title: "Success",
                  text: response.message,
                  type: "success",
                  showCancelButton: false,
                  confirmButtonColor: "#007bff",
                  confirmButtonText: "Okay",
                },
                function(isConfirm){
                  if (isConfirm) {
                    window.location = main_path + '/';
                  }
                });
					showValidator(response.error,'form_new_password');
				}else{
					//<!-- your error message or action here! -->
					check_captcha(response.error['g-recaptcha-response']);
					showValidator(response.error,'form_new_password');
				}
			},
			error:function(error){
				console.log(error)
			}
		});
	});
</script>

<script>

$(".g-recaptcha-response").on('input', function(){
	$('.alert-captcha').hide('fast');
	return true;
});


function check_captcha(check_res){
	if (typeof check_res == 'undefined') {
		$('.alert-captcha').hide('fast');
		return true;
	}else{
		if(check_res.length > 0){
			$('.alert-captcha').show('fast');
			$('.alert-captcha').text('Please input captcha field');
			setTimeout(function(){
				$('.alert-captcha').hide('slow');
			},3000);
			return false;
		}

	}
}
</script>
