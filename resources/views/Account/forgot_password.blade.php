<!DOCTYPE html>
<html>
	@include('Layout.header', ['type' => 'home', 'title' => 'Forgot Password', 'icon' => asset('img/logophone.png') ])

<body class="font-base">
	@include('Layout.nav', ['type' => 'home'])
		<div class="container-fluid mobile-margin">
			<div class="row mt-5 mb-3">
				<div class="col-sm-4 text-center">
					<!-- <img src="{{ asset('img/questions.png') }}" class="img-fluid animated slideInLeft" style="width:62%;"> -->
				</div>
				<div class="col-sm-4 p-0">
					<div class="text-center">
						<h4>Company Forgot Password</h4>
						<br>
					</div>
					<div class="card mbt-5 rounded-lg">
						<div class="card-body">
							<form class="needs-validation" id="form_forgot_password" action="{{ url('get_token/1') }}" novalidate>
								<div class="form-row">
									<div class="form-group col-sm-12">
										<label>Email Address </label>
										<input type="text" id="email_address" name="email_address" placeholder="Email Address" class="form-control " required>
										<div class="invalid-feedback" id="err_email_address"></div>
									</div>
									<div class="form-group col-sm-12">
										<div class="row">
											<div class="col-sm-10">
													<p class="alert alert-danger alert-captcha hide" style="width: 90%;"></p>
													<div class="g-recaptcha float-left" data-sitekey="{{ env('CAPTCHA_KEY') }}"></div>
													<div class="invalid-feedback" id="err_g-recaptcha-response"></div
														>
											</div>
											<div class="col-sm-2">
												<span class="form-group btn-login-mobile">
													<button type="submit" class="btn btn-primary btn-sm">Submit</button>
													<a href="https://bona.com.sg/" target="_blank" class="btn btn-dark btn-sm" style="margin: 8px 0;">Support</a>
												</span>
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="text-center mt-4">
						<div class="row">
							<div class="col-sm-12">
								<a href="https://bona.com.sg/" target="_blank" class="btn-sm bold font-base-lg text-dark">Bona ERP by Bona Technologies</a>
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('img/sgd.jpg') }}" alt="" class="img-fluid">
							</div>
							<div class="col-sm-6">
								<img src="{{ asset('img/infocom.jpg') }}" alt="" class="img-fluid">
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 text-center">
					<!-- <img src="{{ asset('img/questions.png') }}" class="img-fluid animated slideInLeft" style="width:62%;"> -->
				</div>
			</div>
		</div>
</body>
	@include('Layout.footer', ['type' => 'home'])
</html>


  <script>
	$("#form_forgot_password").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

    // alert(url);
		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
			},
			success:function(response){
				if(response.status == true){
					// console.log(response);
					swal("Success", response.message, "success");
					showValidator(response.error,'form_forgot_password');
				}else{
					//<!-- your error message or action here! -->
					check_captcha(response.error['g-recaptcha-response']);
					showValidator(response.error,'form_forgot_password');
				}
			},
			error:function(error){
				console.log(error);
			}
		});
	});
</script>

<script type="text/javascript">
$(".g-recaptcha-response").on('input', function(){
	$('.alert-captcha').hide('fast');
	return true;
});


function check_captcha(check_res){
	if (typeof check_res == 'undefined') {
		$('.alert-captcha').hide('fast');
		return true;
	}else{
		if(check_res.length > 0){
			$('.alert-captcha').show('fast');
			$('.alert-captcha').text('Please input captcha field');
			setTimeout(function(){
				$('.alert-captcha').hide('slow');
			},3000);
			return false;
		}

	}
}
</script>
