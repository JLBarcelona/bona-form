<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::namespace('Account')->group(function () {
	Route::get('/', function () {
	    return view('Account/login');
	});
	Route::post('/check_user', 'AccountController@login_user');
	Route::get('/logout/admin','AccountController@logout_admin')->name('logout.admin');


	Route::get('/logout/user','AccountController@logout_user')->name('logout.user');

	// logout_user
	Route::get('/home/register', 'AccountController@register')->name('home.register');

	// registration
 	Route::post('/user/add', 'SignupController@add')->name('user.add');


 	Route::get('/forgot_password', function(){
	    return view('Account.forgot_password');
	  });

	 Route::post('get_token/{user_type}', 'ForgotPasswordController@get_token');
	 Route::get('/new_password/{token}/{id}', 'ForgotPasswordController@new_password')->name('account.new_password');
	 Route::get('/verify_account/{token}/{id}', 'AccountController@verify_account')->name('account.verify');

	 Route::post('/create_new_password/{token}/{id}', 'ForgotPasswordController@create_new_password');
});


Route::namespace('Home')->group(function () {
 	Route::post('/form/add_form', 'HomeController@add_form')->name('form.add');
 	Route::post('/form/email_validator', 'HomeController@email_validator')->name('form.email_validator');
 	// form
	Route::get('/form/edit/{form_id}', 'HomeController@edit_form')->name('form.edit');
 	Route::get('/form/view/{form_id}', 'HomeController@view')->name('form.view');
	Route::get('/form', 'HomeController@take_form')->name('form.new');
	Route::get('/submit/{form_id?}', 'HomeController@submit')->name('form.submit');


	// Application
	Route::get('/application/edit/{application_form_id}', 'ApplicationController@edit_form')->name('application.edit');
	Route::post('/application/add_form', 'ApplicationController@add_form')->name('application.add');
 	Route::get('/application/view/{application_form_id}', 'ApplicationController@view')->name('application.view');
	Route::get('/application', 'ApplicationController@take_form')->name('application.new');
	Route::get('/application/submit/{application_form_id?}', 'ApplicationController@submit')->name('application.submit');

	Route::get('/app/list_app', 'ApplicationController@list_app')->name('app.list');
	Route::get('/app/delete_app/{application_form_id}', 'ApplicationController@delete_app')->name('app.delete');

	Route::get('/WechatApplicationForm/{token}', 'ApplicationController@form_download')->name('form.download');

});


Route::namespace('Admin')->group(function () {
	Route::middleware(['auth:admin'])->group(function () {

   		Route::get('/Admin/config', function(){
			return view('Admin.account_settings');
		})->middleware('auth:admin');


 		Route::get('/Admin/email/config', function(){
			return view('Admin.email_settings');
		});

		Route::get('/email/list_email', 'EmailController@list_email')->name('email.list');
		Route::post('/email/add_email', 'EmailController@add_email')->name('email.add');
		Route::get('/email/delete_email/{email_id}', 'EmailController@delete_email')->name('email.delete');


		// /Admin/email/config

		// Account wechat
		Route::get('/admin/account/form', function(){
			return view('Admin.account');
		})->name('admin.account.form');

		// Application wechat
		Route::get('/admin/application/form', function(){
			return view('Admin.application');
		})->name('admin.application.form');



		Route::get('/admin', 'AdminController@index')->name('admin.index');

		//
		 Route::get('/form/list_form', 'AdminController@list_form')->name('form.list');
		 Route::get('/form/delete_form/{form_id}', 'AdminController@delete_form')->name('form.delete');

		// Account
		 Route::get('/account/list', 'AccountController@list')->name('account.list');
		 Route::post('/account/add', 'AccountController@add')->name('account.add');
		 Route::get('/account/delete/{id}', 'AccountController@delete_account')->name('account.delete');

		 // Account Password
		 Route::post('/password/change_password', 'AccountController@change_password')->name('password.change_password');

	});
});
